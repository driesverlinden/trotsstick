<?php
	/**
	 * Wie ben ik_edit page for TrotsStick
	 *
	 * @author	Andries Verlinden <andriesverlinden@gmail.com>
	 * @version	1.8
	 */


	/**
	 * Includes
	 * ----------------------------------------------------------------
	 */

	    // config & functions
	    require_once './core/includes/config.php';
	    require_once './core/includes/functions.php';

	    // needed classes
	    require_once './core/includes/classes/database.php';
	    require_once './core/includes/classes/template.php';


	/**
	 * Database connection
	 * ----------------------------------------------------------------
	 */
	    $db = new TrotsStick(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	    $db->connect();


	/**
	 * Start session and check if we are logged in
	 * ----------------------------------------------------------------
	 */
	    // start a session
	    session_start();

	    // are we logged in or not
	    $loggedIn = isset($_SESSION['loggedin']) ? $_SESSION['loggedin'] : false;
	    $user = isset($_SESSION['login']) ? $_SESSION['login'] : '';

	    if ($loggedIn === false) {
		header('location: index.php');
		exit();
	    }


	/**
	 * Initial Values
	 * ----------------------------------------------------------------
	 */

	    // member related params
	    $myMemberId = isset($_SESSION['login']) ? $_SESSION['login'] : 'stranger'; // value from login or sth
	    $myBasePage = '/red';
	    $myBaseDir = dirname(__FILE__) . '/members/' . $myMemberId . $myBasePage ;	// Path where files are store

	    // user records
	    $name = '';
	    $street = '';
	    $city = '';
	    $phone = '';
	    $email = '';
	    $hobby = '';
	    $talent = '';

	    // clear error messages
	    $msgName = '';
	    $msgStreet = '';
	    $msgCity = '';
	    $msgPhone = '';
	    $msgEmail = '';
	    $msgYou = '';
	    $msgFamily = '';
	    $msgFamily2 = '';
	    $msgHobby = '';
	    $msgPicHobby = '';
	    $msgTalent = '';
	    $msgPicTalent = '';

	/**
	 * Read user records
	 * ----------------------------------------------------------------
	 */

	    $userrecords = $db->retrieveOne('SELECT * FROM userrecords WHERE id="0"');

	    if (count($userrecords) > 0) {
		$name =	    $userrecords['name'];
		$street =   $userrecords['address'];
		$city =	    $userrecords['city'];
		$phone =    $userrecords['phone'];
		$email =    $userrecords['mail'];
		$hobby =    $userrecords['hobby'];
		$talent =   $userrecords['talent'];
	    }

	    // save all records
	    $name  = (isset($_POST['name']) ? ((string) $_POST['name']) : $name);
	    $street = (isset($_POST['street']) ? ((string) $_POST['street']) : $street);
	    $city = (isset($_POST['city']) ? ((string) $_POST['city']) : $city);
	    $phone = (isset($_POST['phone']) ? ((string) $_POST['phone']) : $phone);
	    $email = (isset($_POST['email']) ? ((string) $_POST['email']) : $email);
	    $hobby = isset($_POST['hobby']) ? (string) $_POST['hobby'] : $hobby;
	    $talent = isset($_POST['talent']) ? $_POST['talent'] : $talent;

	/**
	 * Handle action 'btnCancel' (user pressed cancel button)
	 * ----------------------------------------------------------------
	 */

	    if (isset($_POST['btnCancel'])) {
		header('Location: wiebenik.php');
		exit(0);
	    }


	/**
	 * Handle action 'btnEdit' (user pressed register button)
	 * ----------------------------------------------------------------
	 */
		
	    if (isset($_POST['btnEdit'])) {

		// allOk?
		$allOk = true;

		// regexes we'll need
		$rex	    = '/^[\w]+$/i';	// any word character (1 or more)
		$rexName    = '/^[^<"\'>;]+$/';	// no ^ < " ' > or ;
		$rexStreet  = '/^[^<"\'>;]+$/';	// no ^ < " ' > or ;
		$rexCity    = '/^[^<"\'>;]+$/';	// no ^ < " ' > or ;
		$rexPhone   = '/^[0-9]+$/i';	// 0-9
		$rexEmail   = '/^[\w+\.\+-_]+@(([\w\+-])+\.)+[a-z]{2,4}$/i';	// e-mailaddress

		// check name
		    // check syntax (regex)
		    if (!preg_match($rexName, $name)) {
			$msgName = 'Je mag geen ^ < " \' > of ; gebruiken in je naam.';
			$allOk = false;
		    }

		// check address
		    // check syntax (regex)
		    if (!preg_match($rexStreet, $street)) {
			$msgStreet = 'Je mag geen ^ < " \' > of ; gebruiken in je adres.';
			$allOk = false;
		    }

		// check city
		    // check syntax (regex)
		    if (!preg_match($rexCity, $city)) {
			$msgCity = 'Je mag geen ^ < " \' > of ; gebruiken in je stad of gemeente.';
			$allOk = false;
		    }

		// check phone
			if ($phone == "") {
				$allOk = true;
			} else { 
				// check syntax (regex)
		    	if (!preg_match($rexPhone, $phone)) {
					$msgPhone = 'Je mag enkel cijfers gebruiken in je telefoonnummer, dus geen spaties tussen de cijfers.';
					$allOk = false;
		    	}
			}

		// check e-mail
			if ($email == "") {
				$allOk = true;
			} else {
				// check syntax (regex)
		    	if (!preg_match($rexEmail, $email)) {
					$msgEmail = 'Gelieve een geldig e-mailadres op te geven.';
					$allOk = false;
		    	}
			}
		    

		// check photo you
		    // Emtpy field?
		    if (!empty($_FILES['you'])) {
			if ($_FILES['you']['name'] != '') {
			   $nameFile = $_FILES['you']['name'];
			   if(!isPic($nameFile)) {
				$allOk = false;
				$msgYou = 'Je kan enkel foto\'s met de extensie\'s .jpg, .jpeg, .png en .gif uploaden.';
			    }
			}
		    }
		    
		// check photo family
		    // Emtpy field?
		    if (!empty($_FILES['family'])) {
			if ($_FILES['family']['name'] != '') {
			    $nameFile = $_FILES['family']['name'];
			    if(!isPic($nameFile)) {
				$allOk = false;
				$msgFamily = 'Je kan enkel foto\'s met de extensie\'s .jpg, .jpeg, .png en .gif uploaden.';
			    }
			}
		    }

		// check photo family2
		    // Emtpy field?
		    if (!empty($_FILES['family2'])) {
			if ($_FILES['family2']['name'] != '') {
			    $nameFile = $_FILES['family2']['name'];
			    if(!isPic($nameFile)) {
				$allOk = false;
				$msgFamily = 'Je kan enkel foto\'s met de extensie\'s .jpg, .jpeg, .png en .gif uploaden.';
			    }
			}
		    }

		// check hobby
		    // no check needed

		// check photo hobby
		    // Emtpy field?
		    if (!empty($_FILES['picHobby'])) {
			if ($_FILES['picHobby']['name'] != '') {
			    $nameFile = $_FILES['picHobby']['name'];
			    if(!isPic($nameFile)) {
				$allOk = false;
				$msgPicHobby = 'Je kan enkel foto\'s met de extensie\'s .jpg, .jpeg, .png en .gif uploaden.';
			    }
			}
		    }

		// check talent
		    // no check needed

		// check photo talent
		    // Emtpy field?
		    if (!empty($_FILES['picTalent'])) {
			if ($_FILES['picTalent']['name'] != '') {
			    $nameFile = $_FILES['picTalent']['name'];
			    // check if the photo is a JPG
			    if(!isPic($nameFile)) {
				$allOk = false;
				$msgPicTalent ='Je kan enkel foto\'s met de extensie\'s .jpg, .jpeg, .png en .gif uploaden.';
			    }
			}
		    }

		// process result
		    // all is good!
		    if ($allOk === true) {

			// save user
			saveUserRecords($name, $street, $city, $phone, $email, $hobby, $talent);

			// save photo you
			if (!empty($_FILES['you'])) {

			    $fileToCopy = $_FILES['you']['tmp_name'];
			    $fileName   = $_FILES['you']['name'];

			    if (isPic($fileName)) {
				$copyTo = $myBaseDir . '/you.jpg';
				@move_uploaded_file($fileToCopy, $copyTo) or showError('cantUploadFile', $copyTo . $fileName);
			    } else {
				$msgYou = 'Je kan enkel foto\'s met de extensie\'s .jpg, .jpeg, .png en .gif uploaden.';
			    }
			}

			// save photo family
			if (!empty($_FILES['family'])) {

			    $fileToCopy = $_FILES['family']['tmp_name'];
			    $fileName   = $_FILES['family']['name'];

			    if (isPic($fileName)) {
				$copyTo = $myBaseDir . '/family.jpg';
				@move_uploaded_file($fileToCopy, $copyTo) or showError('cantUploadFile', $copyTo . $fileName);
			    } else {
				$msgFamily = 'Je kan enkel foto\'s met de extensie\'s .jpg, .jpeg, .png en .gif uploaden.';
			    }
			}

			// save photo family2
			if (!empty($_FILES['family2'])) {

			    $fileToCopy = $_FILES['family2']['tmp_name'];
			    $fileName   = $_FILES['family2']['name'];

			    if (isPic($fileName)) {
				$copyTo = $myBaseDir . '/family2.jpg';
				@move_uploaded_file($fileToCopy, $copyTo) or showError('cantUploadFile', $copyTo . $fileName);
			    } else {
				$msgFamily2 = 'Je kan enkel foto\'s met de extensie\'s .jpg, .jpeg, .png en .gif uploaden.';
			    }
			}

			// save photo hobby
			if (!empty($_FILES['picHobby'])) {

			    $fileToCopy = $_FILES['picHobby']['tmp_name'];
			    $fileName   = $_FILES['picHobby']['name'];

			    if (isPic($fileName)) {
				$copyTo = $myBaseDir . '/hobby.jpg';
				@move_uploaded_file($fileToCopy, $copyTo) or showError('cantUploadFile', $copyTo . $fileName);
			    } else {
				$msgHobby = 'Je kan enkel foto\'s met de extensie\'s .jpg, .jpeg, .png en .gif uploaden.';
			    }
			}

			// save photo talent
			if (!empty($_FILES['picTalent'])) {

			    $fileToCopy = $_FILES['picTalent']['tmp_name'];
			    $fileName   = $_FILES['picTalent']['name'];

			    if (isPic($fileName)) {
				$copyTo = $myBaseDir . '/talent.jpg';
				@move_uploaded_file($fileToCopy, $copyTo) or showError('cantUploadFile', $copyTo . $fileName);
			    } else {
				$msgYou = 'Je kan enkel foto\'s met de extensie\'s .jpg, .jpeg, .png en .gif uploaden.';
			    }
			}

			// redirect
			header('location: wiebenik.php');
			exit();
		    }
	    }


	/**
	 * No action to handle: show our page itself
	 * -------------------------------------------------------------
	 */

	    // Main Layout

		// load main layout into a template
		$mainTpl = new Template('./core/layout/layout.tpl');

		// asisgn vars in our main layout tpl
		$mainTpl->assign('pageTitle', 	'TrotsStick - Wie ben ik? (wijzig)');
		$mainTpl->assign('pageMeta', 	'');
		$mainTpl->assign('pageCss',		'');
		$mainTpl->assign('pageJs',	'<!-- TinyMCE -->
	    <script type="text/javascript" src="core/js/tiny_mce/tiny_mce.js"></script>
	    <script type="text/javascript">
		tinyMCE.init({
		    mode : "textareas",
		    theme : "advanced",
		    theme_advanced_buttons1_add : "fontselect, bullist, numlist, indent, outdent, undo, redo, link, unlink, cleanup, removeformat",
		    theme_advanced_buttons2 : "",
		    theme_advanced_buttons3 : "",
		    theme_advanced_disable : "sub, sup, seperator, code, image, hr, anchor, formatselect, styleselect, fontsizeselect, forecolor, backcolor, forecolorpicker, backcolorpicker, charmap, visualaid, newdocument, blockquote, help",
		    theme_advanced_toolbar_location : "top",
		    theme_advanced_toolbar_align : "left",
		    theme_advanced_fonts : "Andale Mono=andale mono,times;" + "Arial=arial,helvetica,sans-serif;" + "Arial Black=arial black,avant garde;" + "Book Antiqua=book antiqua,palatino;" + "Comic Sans MS=comic sans ms,sans-serif;" + "Courier New=courier new,courier;" + "Georgia=georgia,palatino;" + "Helvetica=helvetica;" + "Impact=impact,chicago;" + "Symbol=symbol;" + "Tahoma=tahoma,arial,helvetica,sans-serif;" + "Terminal=terminal,monaco;" + "Times New Roman=times new roman,times;" + "Trebuchet MS=trebuchet ms,geneva;" + "Verdana=verdana,geneva;" + "Webdings=webdings;" + "Wingdings=wingdings,zapf dingbats"
		});
	    </script>
	    <!-- /TinyMCE -->');
		$mainTpl->assign('pageH2',	'Wie ben ik? (wijzig)');

		// show logged in user
		if (($loggedIn == true)) {
		    $mainTpl->assignOption('oLoggedIn');
		    $mainTpl->assign('login', $_SESSION['login']);
		}


	    // Page specific template

		// new template
		$pageTpl = new Template('./core/layout/edit_wiebenik.tpl');

		// assign variables of the form
		$pageTpl->assign('formAction', 	$_SERVER['PHP_SELF']);

		// assign user records
		$pageTpl->assign('name',    $name);
		$pageTpl->assign('street',  $street);
		$pageTpl->assign('city',    $city);
		$pageTpl->assign('phone',   $phone);
		$pageTpl->assign('email',   $email);
		$pageTpl->assign('hobby',   $hobby);
		$pageTpl->assign('talent',  $talent);

		// assign error messages
		$pageTpl->assign('msgName',	$msgName);
		$pageTpl->assign('msgStreet',	$msgStreet);
		$pageTpl->assign('msgCity',	$msgCity);
		$pageTpl->assign('msgPhone',	$msgPhone);
		$pageTpl->assign('msgEmail',	$msgEmail);
		$pageTpl->assign('msgYou',	$msgYou);
		$pageTpl->assign('msgFamily',	$msgFamily);
		$pageTpl->assign('msgFamily2',	$msgFamily2);
		$pageTpl->assign('msgHobby',	$msgHobby);
		$pageTpl->assign('msgPicHobby',	$msgPicHobby);
		$pageTpl->assign('msgTalent',	$msgTalent);
		$pageTpl->assign('msgPicTalent',$msgPicTalent);


	    // Parse page specific layout into main layout
		$mainTpl->assign('pageContent', $pageTpl->getContent());

	    // Output our main layout
		$mainTpl->display();

		
//EOF