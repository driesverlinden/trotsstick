***************************************************
					TROTSSTICK
***************************************************

TrotsStick is een realisatie van het OVSG.
programmeur: Andries Verlinden
versie: v1.9
versie USBWebserver: 8.5

***************************************************

Verbeteringen bij v1.9
-----------------------

- update USBWebserver (versie 8.2 -> 8.5)

Verbeteringen bij v1.8.3 (kleine wijzigingen aan eindversie)
-----------------------

- Wie ben ik? - niet kijken naar naam (enkel serverversie)
- typfouten wegwerken

Verbeteringen bij v1.8.2 (kleine wijzigingen aan eindversie)
-----------------------

- probleem opgelost met doorlopende lange tekst in tekstvakken
- std foto bij Wie ben ik? en Super kan niet verwijderd worden
- aanpassing delete-functie Wie Ben Ik?
- verdwijnen afspelen video player wegens niet werkend
- wijziging naam -> id (serverversie 1.5)
- vermelding WAI-Pass op About

Verbeteringen bij v1.8  (eindversie)
-----------------------

- verdwijnen / wijzigen pagina's
- superknap/trots foto kunnen uploaden + verwijderen
- controleren op updates weg
- aanpassen tekst About
- verdwijnen van delete-functie .Trashes wegens geen nut (Win <-> Mac)

Verbeteringen bij v1.7
-----------------------

- Wie ben ik? opnieuw editbaar (fout bij MySQL)
- telefoonnr en e-mailadres zijn niet meer verplicht bij Wie ben ik? (fout PHP)
- stripPostSlashes bij hobby en talent
- verwijderen foto's
- autologin na configuratie TrotsStick


Verbeteringen bij v1.6 (grote testfase 1)
-----------------------

- extra beveiliging gebruikersnaam <-> wachtwoord (min. 6 karakters)
- integratie van MySQL
- verplaatsen templates
- verplaatsen uploadformulier -> dichterbij bestanden
- leegmaken Trashes (optie in About)
- profiel editeerbaar gemaakt (probleem Firefox)
- lettertype tekstvakken aangepast + verschijnen/verdwijnen tekstvakken
- mogelijk om tweede foto toe te voegen bij Wie ben ik? 
- integreren handleiding (drive én core)

Verbeteringen bij v1.5
-----------------------

- vermijden error bij Wie ben ik? (fread textFileSize)
- link naar About op Wie ben ik?
- toevoegen Handleiding
- aanpassing eerste zin bij Taalknap
- wijziging naam exe naar START TROTSSTICK.exe

Verbeteringen bij v1.4 (tweede evaluatie)
-----------------------

- aanpassingen na tweede evaluatie:
	+ aanpassing TinyMCE-tekstvakken
	+ toevoeging taal-, reken-, ...-knap pagina's
	+ mogelijkheid van toevoegen hobby's en talenten /+ foto's	
	+ introductie beren bij Mijn leren
- opkuisen codebestanden
- versterken beveiliging

Verbeteringen bij v1.3
-----------------------

- tweede publieke versie

Verbeteringen bij v1.2
-----------------------

- aanpassen layout About
- weergave van grootte usb-stick

Verbeteringen bij v1.1 (eerste evaluatie)
-----------------------

- aanpassingen na eerste evaluatie: 
	+ mogelijkheid tot volledige naam bij login (registratie TrotsStick)
	+ alle tekst in tekstvakken wordt weergegeven (limiet bij inlezen bestand)
	+ 'Versturen' => 'Uploaden'
	+ 'Pas aan' => 'OK'
	+ upload gebeurt per kleur
	+ 'Waar moet ik nog aan werken?' valt weg
	+ bij 'Mijn leren', 'Waar ben ik knap in?' en 'Hoe ben ik in de groep?' 
	  een tweede tekstvak waar men de mening van de klasgenoten kan bewaren.
- bij Wie ben ik? (Edit-mode) is het niet verplicht om alle velden in te vullen.

Verbeteringen bij v1.0
-----------------------

- eerste publieke versie
- titellinks 
- taal aanpassen
- aanpassen naar lege modus
- error pagina
- reeds bestaande gegevens in userrecords worden weergegeven bij edit_wiebenik

Verbeteringen bij v0.3
-----------------------

- aboutpagina
- aanpassen naar min 1024x678-resolutie

Verbeteringen bij v0.2
-----------------------

- upload problemen
- verwijderen niet gebruikte variabelen
- slechts één gebruiker mogelijk
- logingegevens onthouden in cookie
- herschikkingen spin
- straat en gemeente worden gesplitst 
- Wie ben ik (edit)
- taal aanpassen
- Accepteren van gif
- limiet op bestandsnaam (43 tekens)
- limiet op upload (32 MB)
- verwijderen van bestanden
- geen autorun.inf vanaf Windows 7
