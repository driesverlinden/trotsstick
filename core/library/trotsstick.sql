-- phpMyAdmin SQL Dump
-- version 3.2.5
-- http://www.phpmyadmin.net
--
-- Machine: localhost
-- Genereertijd: 04 Jun 2011 om 12:35
-- Serverversie: 5.1.44
-- PHP-Versie: 5.2.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `trotsstick`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `userrecords`
--

CREATE TABLE `userrecords` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `hobby` longtext NOT NULL,
  `talent` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Gegevens worden uitgevoerd voor tabel `userrecords`
--


-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `name` varchar(20) NOT NULL COMMENT 'name user',
  `password` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Gegevens worden uitgevoerd voor tabel `users`
--


-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `whey`
--

CREATE TABLE `whey` (
  `id` int(11) NOT NULL,
  `waar_knap` longtext NOT NULL,
  `waar_trots` longtext NOT NULL,
  `mijn_toekomst` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Gegevens worden uitgevoerd voor tabel `whey`
--

INSERT INTO `whey` VALUES(0, '', '', '');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `wheyothers`
--

CREATE TABLE `wheyothers` (
  `id` int(11) NOT NULL,
  `waar_knap` longtext NOT NULL,
  `mijn_toekomst` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Gegevens worden uitgevoerd voor tabel `wheyothers`
--

INSERT INTO `wheyothers` VALUES(0, '', '');
