    <h3>Over de TrotsStick</h3>

    <div class="framework">
	<p>Beste TrotsStick-gebruiker</p>
	<p>Jij gaat nu de grote overstap maken van de Trotsdoos naar de TrotsStick. Proficiat! Ik ben nu al trots op jou!</p>
	<p>
	    Ik zou de Trotsdoos toch nog niet te ver weg zetten, want ik ben er zeker van dat de TrotsStick een plaatsje zal vinden in de Trotsdoos. Sommige voorwerpen bewaar je beter in je Trotsdoos, andere ervaringen of talenten kun je dan weer op je TrotsStick bewaren.<br />
	    De TrotsStick geeft je de kans om jezelf voor te stellen, om je talenten te verzamelen, om te laten zien wat je al kan, om te beslissen waaraan je wil of moet werken.
	</p>
	<p>Alleen jij kan je TrotsStick openen met een wachtwoord. Misschien wil je je TrotsStick ook wel eens laten zien aan een vriend of een vriendin, aan je ouders, aan je leraar, aan iemand van je familie ...</p>
	<p>Vanaf nu ben jij de trotse eigenaar van je persoonlijke TrotsStick. Draag er zorg voor en maak regelmatig wat tijd vrij om hem aan te vullen.</p>
	<form action="about.php" method="get">
	    <p><input type="submit" id="btnBack" name="btnBack" value="Open jouw TrotsStick" /></p>
   	</form>
    </div>

    <h4>Jouw TrotsStick</h4>
    <p>Jouw TrotsStick gebruikt momenteel <strong>versie {$version}</strong>. Deze werd uitgebracht op <strong>{$lastUpdate}</strong>.</p>
    </p>
    <p class="framework">Jouw USB-stick is {$total} GB. Je hebt nog plaats voor <strong>{$free} GB</strong>.</p>
    <p>&nbsp;</p>
    <p><a href="core/manual/Handleiding_TrotsStick.pdf" target="_blank">Handleiding TrotsStick</a> (pdf)</p>
    <p><a href="http://www.youtube.com/watch?v=Mo6Z67d6GSU" target="_blank">Videohandleiding TrotsStick</a> (YouTube)</p>
    <form action="about.php" method="get">
		<p><input type="submit" id="btnBack" name="btnBack" value="Open jouw TrotsStick" /></p>
    </form>

    <h5 class="blankspace">Disclaimer</h5>
    <h5 class="normal">De TrotsStick is een idee van <a href="mailto:jos.vandael@ovsg.be">Jos Vandael</a>, pedagogisch medewerker van het <a href="http://www.ovsg.be">Onderwijssecretariaat van de Steden en Gemeenten van de Vlaamse Gemeenschap</a> (OVSG).</h5>
    <h5 class="normal">
	De TrotsStick werd gemaakt door <a href="mailto:andriesverlinden@hotmail.com">Andries Verlinden</a> en <a href="mailto:danny.verlinden@ovsg.be">Danny Verlinden</a> naar een design van <a href="http://www.wai-pass.be">WAI-Pass</a>.<br />
	Het programma maakt gebruik van de gratis <a href="http://www.usbwebserver.net">USBWebserver-applicatie</a>. <br />
	De broncode achter de TrotsStick is auteurrechtelijk beschermd en mag niet gekopieerd worden zonder de toestemming van de auteurs en vermelding van hun naam.
    </h5>		
  