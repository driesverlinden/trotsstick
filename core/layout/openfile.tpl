
    {option:oNoFileSelected}
    <h2>Geen bestand geselecteerd</h2>
    <p>Je hebt blijkbaar geen bestand geselecteerd. Je kan dus ook niets openen.</p>

    <h2>Keer terug</h2>
    <p>Als je wilt terugkeren naar je TrotsStick, klik dan op <a href="index.php">deze link</a>.</p>
    {/option:oNoFileSelected}

    {option:oOpen}
	{option:oDoc}
	    <h3>Een document openen</h3>

	    <h4>Stap 1</h4>
	    <p>Om het bestand te openen en te bewerken, moet je het eerste downloaden naar je lokale computer.</p>
	    <p>download <a href="{$urlFile}">{$file}</a></p>
	    <h4>Stap 2</h4>
	    <p>Nu open je het bestand met het gepaste programma op je computer. Voor een tekstdocument kan dit bijvoorbeeld Word zijn.</p>
	    <h4>Stap 3</h4>
	    <p>Als je klaar bent met het document, kan je het hier opnieuw uploaden.</p>
	    <form action="{$formAction}" method="post" id="formUploadOpen">
		<div id="upload">
		    <input type="file" name="upload" id="upload" />
		    <input type="submit" name="btnUpload" id="btnUpload" />
		</div>
	    </form>
	    <h4>Keer terug</h4>
	    <p>Als je wilt terugkeren naar je TrotsStick, klik dan op <a href="index.php">deze link</a>.</p>
	{/option:oDoc}

	{option:oPic}
	    <h3>Een afbeelding bekijken</h3>

	    <p><img src="{$urlFile}" alt="" width=500 /></p>
	    <h4>De afbeelding downloaden</h4>
	    <p>Je kan de afbeelding ook bewaren op je lokale computer.</p>
	    <p>download <a href="{$urlFile}">{$file}</a></p>
	    <h4>Keer terug</h4>
	    <p>Als je wilt terugkeren naar je TrotsStick, klik dan op <a href="index.php">deze link</a>.</p>
	{/option:oPic}

	{option:oMov}
	    <h3>Een filmpje bekijken</h3>
	    <video src="{$urlFile}" media="video/wmv" controls="controls">Je browser ondersteunt de HTML5 video tag niet.</video>
	    <h4>het filmpje downloaden</h4>
	    <p>Je kan het filmpje bewaren op je lokale computer.</p>
	    <p>download <a href="{$urlFile}">{$file}</a></p>
	    <h4>Keer terug</h4>
	    <p>Als je wilt terugkeren naar je TrotsStick, klik dan op <a href="index.php">deze link</a>.</p>
	{/option:oMov}

	{option:oSou}
	    <h3>Een geluidsbestand beluisteren</h3>

	    <embed src="{$urlFile}" width=200 height=35 autostart=false repeat=true loop=true> </embed>
	    <h4>het geluidsbetand downloaden</h4>
	    <p>Je kan het geluidsbestand ook bewaren op je lokale computer.</p>
	    <p>download <a href="{$urlFile}">{$file}</a></p>
	    <h4>Keer terug</h4>
	    <p>Als je wilt terugkeren naar je TrotsStick, klik dan op <a href="index.php">deze link</a>.</p>
	{/option:oSou}

	{option:oFile}
	    <h3>Een bestand openen</h3>
	    <p>download <a href="{$urlFile}">{$file}</a></p>
	    <h4>Keer terug</h4>
	    <p>Als je wilt terugkeren naar je TrotsStick, klik dan op <a href="index.php">deze link</a>.</p>
	{/option:oFile}
    {/option:oOpen}