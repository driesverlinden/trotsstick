    <h3>Zelfknap</h3>

    <ul>
      <li><strong>Ik kan goed alleen bezig zijn</strong> (zelfstandig).</li>
      <li>Ik denk na over eigen gedrag, gevoelens en dergelijke.</li>
      <li>Ik heb soms een uitgesproken mening over wat ik belangrijk vind, graag wil doen, graag wil hebben, en ik ben vastberaden en doelgericht.</li>
      <li>Ik moet me niet altijd kunnen uiten over wat ik denk, voel of heb meegemaakt.</li>
      <li>Ik vind het interessant om de eigen voorkeuren en sterkere/zwakkere kanten te kennen.</li>
      <li>Ik houd een dagboek bij of maak op een andere manier aantekeningen over dingen die gebeuren en wat ik daarvan vind.</li>
      <li>Ik kan opgaan in fantasie, dagdromen.</li>
      <li>Ik heb graag een &ldquo;priv&eacute;plek&rdquo; om te werken, studeren, lezen en dergelijke.</li>
      <li>Ik ben me bewust van eigen gevoelens en stemmingen en heb de neiging die te willen be&iuml;nvloeden.</li>
      <li>Ik heb dikwijls denktijd nodig alvorens op iets te reageren.</li>
      <li>Ik neem graag verantwoordelijkheid.</li>
      <li>Ik stel meestal hoge eisen aan mezelf.</li>
      <li>Ik ben wat filosofisch ingesteld.</li>
      <li>Ik toon sterk gevoel van onafhankelijkheid/sterke wil.</li>
      <li>Ik kan goed naar anderen luisteren, voel gevoelens en stemmingen van een ander goed aan.</li>
    </ul>
    <p><em>Bron: Vrij naar &ldquo;De kracht van meervoudige intelligentie&rdquo; (www.bazalt.nl)</em></p>
    <p><a href="waarknap.php">Terug</a></p>
    
  