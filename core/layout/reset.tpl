    <h2>Wachtwoord resetten</h2>
    {option:oStep1}
    <h3>Stap 1 / 4</h3>
    <p>
	Via deze pagina kan je je wachtwoord resetten.<br />
	Geef allereerste je gebruikersnaam op zodat we jouw geheime vraag kunnen ophalen.
    </p>
    <form action="{$formAction}" method="post">
	<dl class="clearfix">
	    <dt>je gebruikersnaam:</dt>
	    <dd>
		<input type="text" id="name" name="name" value="" />
		<span class="message error" id="msgName" name="msgName">{$msgName|htmlentities}</span>
	    </dd>
	    <dt class="full clearfix buttonrow">
		<input type="submit" id="btnSendName" name="btnSendName" value="Haal vraag op" />
	    </dt>
	</dl>
    </form>
    {/option:oStep1}

    {option:oStep2}
    <h3>Stap 2 / 4</h3>
	Beantwoord onderstaande vraag.
    </p>

    <h3>{$question|htmlentities}</h3>
    <form action="{$formAction}" method="post">
	<dl class="clearfix">
	    <dt>jouw antwoord:</dt>
	    <dd>
		<input type="text" id="answer" name="answer" value="" />
		<span class="message error" id="msgAnswer" name="msgAnswer">{$msgAnswer|htmlentities}</span>
	    </dd>
	    <dt class="full clearfix buttonrow">
		<input type="submit" id="btnReset" name="btnReset" value="Reset wachtwoord" />
		<input type="hidden" id="name" name="name" value="{$name|htmlentities}" />
	    </dt>
	</dl>
    </form>
    {/option:oStep2}
    
    {option:oStep3}
    <h3>Stap 3 / 4</h3>
    <p>Je kan nu een nieuw wachtwoord kiezen.</p>
    <form action="{$formAction}" method="post">
	<dl class="clearfix">
	    <dt>je nieuw wachtwoord:</dt>
	    <dd>
		<input type="text" id="newPw" name="newPw" value="" />
		<span class="message error" id="msgNewPw" name="msgNewPw">{$msgNewPw|htmlentities}</span>
	    </dd>
	    <dt class="full clearfix buttonrow">
		<input type="submit" id="btnRenew" name="btnRenew" value="Wijzig je wachtwoord" />
		<input type="hidden" id="name" name="name" value="{$name|htmlentities}" />
	    </dt>
	</dl>
    </form>
    {/option:oStep3}

    {option:oStep4}
    <h3>Stap 4 / 4</h3>
    <p>Je nieuw wachtwoord werd ingesteld.</p>
    <p>Je kan vanaf nu <a href="login.php">inloggen</a> met je nieuw wachtwoord.</p>
    {/option:oStep4}
