    <h3>Doeknap</h3>

    <ul>
      <li>Ik grijp allerlei aanleidingen aan om te kunnen tekenen, schilderen, eventueel fotograferen.</li>
      <li>Ik heb oog voor verhoudingen, bij het tekenen van mensfiguren.</li>
      <li>Ik blijk details te zien en weer te geven, ook op de achtergrond.</li>
      <li><strong>Ik gebruik materiaal</strong> (als kleurkrijt, potlood, enzovoort) verrassend &ldquo;anders&rdquo;.</li>
      <li>Ik heb het nodig iets te zien om het te begrijpen: afbeelding, demonstratie, voordoen.</li>
      <li>Ik zie van alles voor mij als ik lees, als ik voorgelezen word, als iemand iets vertelt.</li>
      <li>Ik kan fantaseren door mij iets voor te stellen.</li>
      <li>Ik heb interesse voor de inrichting van een ruimte (klas, eigen kamer, enzovoort).</li>
      <li>Ik weet goed de weg in de buurt (kortere weggetjes en dergelijke).</li>
      <li>Ik kan me ori&euml;nteren in een nieuwe omgeving, al dan niet met behulp van een kaart.</li>
      <li>Ik teken figuren om iets vast te houden.</li>
      <li>Ik ben beeldend bezig, ik ontwerp, ik schets, ik bouw en ik construeer.</li>
      <li>Ik kleed me vaak op een eigen manier.</li>
      <li>Ik let op het uiterlijk bij anderen (complimenten).</li>
      <li>Ik doe graag videospelletjes.</li>
    </ul>
    <p><em>Bron: Vrij naar &ldquo;De kracht van meervoudige intelligentie&rdquo; (www.bazalt.nl)</em></p>
    <p><a href="waarknap.php">Terug</a></p>
    
  