    <h3>Natuurknap</h3>

    <ul>
      <li><strong>Ik voel me aangetrokken tot de natuur</strong> (meer dan normale interesse), ik kom op voor de natuur.</li>
      <li>Ik verzamel: breng van alles mee (stenen, schelpen, mos, paardenkiezen, enzovoort).</li>
      <li>Ik heb interesse in natuurverschijnselen, het weer, de ruimte.</li>
      <li>Ik wil namen weten van planten, dieren, gesteenten, natuurgebieden, wolken, lichaamsdelen, enzovoort.</li>
      <li>Ik heb de neiging tot scherp observeren van mensen, dieren, planten, gebeurtenissen.</li>
      <li>Ik zie kleine verschillen en overeenkomsten.</li>
      <li>Ik ben goed in staat om te ordenen en kwalificeren.</li>
      <li>Ik hou ervan om met dieren om te gaan en ze dingen aan te leren (inlevingsvermogen).</li>
      <li>Ik kan belangstelling en talent hebben voor koken.</li>
      <li>Ik weet veel over planten, dieren, hoe ze groeien, hoe ze zich gedragen, wat ze be&iuml;nvloedt en dergelijke. Ik lees daarover, vraag daarover of kijk daarover tv.</li>
      <li>Ik heb interesse in verandering, groei en opeenvolging.</li>
      <li>Ik vertel graag over die aspecten van de natuur waarvoor ik mij interesseer.</li>
      <li>Ik ben sterk gericht op buiten.</li>
      <li>Ik kom vaak met leuke en boeiende ontdekkingen van buiten.</li>
      <li>Ik hou van kamperen.</li>
    </ul>
    <p><em>Bron: Vrij naar &ldquo;De kracht van meervoudige intelligentie&rdquo; (www.bazalt.nl)</em></p>
    <p><a href="waarknap.php">Terug</a></p>
    
  