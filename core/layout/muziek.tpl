    <h3>Muziekknap</h3>

    <ul>
      <li>Ik zing, neurie of fluit (onbewust) voor mijzelf.</li>
      <li><strong>Ik heb graag muziek om me heen</strong>, achtergrondmuziek werkt ontspannend, stimulerend.</li>
      <li>Ik trommel, tik, beweeg ritmisch (bij muziek of op basis van een innerlijk ritme).</li>
      <li>Ik herken instrumenten, stemmen, zangers/groepen, componisten.</li>
      <li>Ik hoor bewust geluiden om me heen (verkeer, natuur, mensen op de achtergrond).</li>
      <li>Ik bespeel een instrument.</li>
      <li>Ik zing snel een melodie na, onthoud liedjes goed.</li>
      <li>Ik reageer duidelijk op verschillen in intonatie als iemand spreekt.</li>
      <li>Ik vertel iets op een boeiende manier (verschil in toonhoogte en tempo).</li>
      <li>Ik hou van meedoen met (of maken van) een yell, rap, liedje.</li>
      <li>Ik maak zelf liedjes en versjes.</li>
      <li>Ik ben gevoelig voor storend geluid (concentratie).</li>
    </ul>
    <p><em>Bron: Vrij naar &ldquo;De kracht van meervoudige intelligentie&rdquo; (www.bazalt.nl)</em></p>
    <p><a href="waarknap.php">Terug</a></p>
    
  