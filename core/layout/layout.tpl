<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="nl">
<head>
    <title>{$pageTitle}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    {$pageMeta}
    <link rel="stylesheet" type="text/css" media="screen" href="core/css/reset.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="core/css/screen.css" />
    <!--[if lte IE 6]><link rel="stylesheet" type="text/css" media="screen" href="/modules/core/layout/css/ie6.css" /><![endif]-->
    <!--[if IE 7]><link rel="stylesheet" type="text/css" media="screen" href="/modules/core/layout/css/ie7.css" /><![endif]-->
    <style type="text/css">
	{$pageCss}
    </style>
	{$pageJs}
</head>
<body>
    <div id="siteWrapper">
	<!-- header -->
	<div id="header">
	    <h1><a href="index.php">TrotsStick</a></h1>
	    <h2>{$pageH2}</h2>
	</div>

	<!-- menubar -->
	<div id="menubar">
	    <ul>
		{option:oLoggedIn}
		<li id="lnklogin"><a href="logout.php">Afmelden</a></li>
		<li id="lnklogin"><a href="index.php">TrotsStick van {$login}</a></li>
		{/option:oLoggedIn}
	    </ul>
	</div>

	<!-- content -->
	<div id="content">
	    {$pageContent}
	</div>

	<!-- footer
	<div id="footer">
	    <ul>
		<li>&copy; Andries Verlinden, 2011</li>
		<li><a href="about.php">disclaimer</a></li>
	    </ul>
	</div> -->

    </div>
	
</body>
</html>