    <h3>Samenknap</h3>

    <ul>
      <li><strong>Ik hou ervan samen met anderen te zijn</strong>.</li>
      <li>Ik neem initiatief tot contact, vraag of een ander mee wilt doen, toon belangstelling.</li>
      <li>Ik kan me inleven/verplaatsen in een ander.</li>
      <li>Ik accepteer andere kinderen zoals ze zijn.</li>
      <li>Ik geef er blijk van iets om een ander te geven.</li>
      <li>Ik heb de neiging leiding op mij te nemen bij spel of werk waardoor anderen (ook) goed kunnen functioneren.</li>
      <li>Ik ben in staat om een positieve atmosfeer op te roepen om de groep bij elkaar te houden.</li>
      <li>Ik ben er op uit conflicten met anderen tot een goede oplossing te brengen.</li>
      <li>Ik wil dingen met anderen delen (materiaal, speelgoed, voedsel, enzovoort).</li>
      <li>Ik let op hoe anderen zich voelen, hoe ze zich gedragen; ik heb de neiging daar op in te spelen.</li>
      <li>Ik help een ander ergens mee.</li>
      <li>Ik kan genieten van bij elkaar zijn, feestjes, uitstapjes, en dergelijke.</li>
      <li>Ik werk graag samen met anderen.</li>
      <li>Ik organiseer gemakkelijk.</li>
      <li>Ik doe buiten de school aan sociale activiteiten of teamsport.</li>
    </ul>
    <p><em>Bron: Vrij naar &ldquo;De kracht van meervoudige intelligentie&rdquo; (www.bazalt.nl)</em></p>
    <p><a href="waarknap.php">Terug</a></p>