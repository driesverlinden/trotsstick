    <h3>Beweegknap</h3>

    <ul>
      <li>Ik gebruik gebaren en gezichtsuitdrukking om iets duidelijk te maken (praten met &ldquo;handen en voeten&rdquo;).</li>
      <li>Ik reageer enthousiast op activiteiten waarbij ik kan bewegen en iets met de handen kan doen.</li>
      <li>Ik heb graag dingen in de hand of raak iets aan om het te kennen of te begrijpen hoe het werkt.</li>
      <li>Ik maak onbewust kleine bewegingen (wippen op de stoel, heen en weer &ldquo;kijken&rdquo;, en dergelijke).</li>
      <li><strong>Ik hou van dansen en bewegen</strong>.</li>
      <li>Ik ben graag praktisch bezig met sleutelen, knutselen, bouwen, dingen klaarzetten.</li>
      <li>Ik voel me aangetrokken tot toneelspelen, iets uitbeelden/uitspelen, mime.</li>
      <li>Ik ben gevoelig voor lichamelijk contact (aanraken, hand geven), en doe dat zelf ook bij volwassenen en medeleerlingen.</li>
      <li>Ik speel graag buiten: rennen, spelletjes, ergens naartoe wandelen of fietsen.</li>
      <li>Ik hou ervan mij lichamelijk in te spannen, te sporten in het algemeen.</li>
      <li>Ik kan goed anderen nadoen (mimiek).</li>
      <li>Ik ben goed in fijne motoriek (computerspelletjes: oog - handco&ouml;rdinatie).</li>
      <li>Doen is begrijpen voor mij.</li>
    </ul>
    <p><em>Bron: Vrij naar &ldquo;De kracht van meervoudige intelligentie&rdquo; (www.bazalt.nl)</em></p>
    <p><a href="waarknap.php">Terug</a></p>
    
  