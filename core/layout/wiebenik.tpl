    <h2 id="whoami"><a href="index.php">Wie ben ik?</a><a href="edit_wiebenik.php" class="edit">wijzig je gegevens</a></h2>

    {option:oNoRecords}
	<h3>Nog geen gegevens!</h3>

	<p>
	    Je hebt je profiel nog niet ingevuld! <br />
	    Je kan dit doen door op het potloodje te klikken naast de titel <em>Wie ben ik?</em> <br />
	    Met het potloodje kun je ook nog later wijzigingen aanbrengen.
	</p>
    {/option:oNoRecords}

    {option:oRecords}
	<h3>{$name}</h3>

	<dl id="dlWhoAmI" class="clearfix">
	    <dt><label>Straat:</label></dt>
	    <dd><label>{$street}</label></dd>

	    <dt><label>Gemeente:</label></dt>
	    <dd><label>{$city}</label></dd>

	    <dt><label>Telefoonnummer:</label></dt>
	    <dd><label>{$phone}</label></dd>

	    <dt><label>E-mailadres:</label></dt>
	    <dd><label>{$email}</label></dd>

	    <dt><label>Mijn hobby's:</label></dt>
	    <dd>{$hobby}</dd>

	    <dt><label>Mijn talenten:</label></dt>
	    <dd>{$talent}</dd>

	    <p id="you">
	    	<a href="{$you}" target="_blank"><img src="{$you}" alt="Jij" height="250" /></a>
	    	{option:oDeleteYou}<a class="delete" href="{$urlDelYou}" title="delete super" onclick="return confirm('Ben je zeker dat je deze foto wilt verwijderen?');">Verwijder</a>{/option:oDeleteYou}
	    </p>
	    <p id="family">
	    	<a href="{$family}" target="_blank"><img src="{$family}" alt="Jouw gezin" height="250" /></a>
	    	{option:oDeleteFamily}<a class="delete" href="{$urlDelFamily}" title="delete super" onclick="return confirm('Ben je zeker dat je deze foto wilt verwijderen?');">Verwijder</a>{/option:oDeleteFamily}
	    </p>
	    {option:oFamily2}
	    <p id="family2">
		<a href="{$family2}" target="_blank"><img src="{$family2}" alt="Jouw gezin" height="250" /></a>
		{option:oDeleteFamily2}<a class="delete" href="{$urlDelFamily2}" title="delete super" onclick="return confirm('Ben je zeker dat je deze foto wilt verwijderen?');">Verwijder</a>{/option:oDeleteFamily2}
	    </p>
	    {/option:oFamily2}
	    <p id="hobby">
		<a href="{$picHobby}" target="_blank"><img src="{$picHobby}" alt="Mijn hobby" height="250" /></a>
		{option:oDeleteHobby}<a class="delete" href="{$urlDelHobby}" title="delete super" onclick="return confirm('Ben je zeker dat je deze foto wilt verwijderen?');">Verwijder</a>{/option:oDeleteHobby}
	    </p>
	    <p id="talent">
		<a href="{$picTalent}" target="_blank"><img src="{$picTalent}" alt="Mijn talent" height="250" /></a>
		{option:oDeleteTalent}<a class="delete" href="{$urlDelTalent}" title="delete super" onclick="return confirm('Ben je zeker dat je deze foto wilt verwijderen?');">Verwijder</a>{/option:oDeleteTalent}
	    </p>
	</dl>
    {/option:oRecords}
    <p class="about"><a href="about.php">De TrotsStick</a></p>