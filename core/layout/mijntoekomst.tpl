    <h2 id="myfuture"><a href="index.php">Mijn toekomst</a></h2>

    {option:oWhey}
    <div id="formWhey">
	<form action="{$formAction}" method="post" id="formWhey">
	    <p><strong>Wat wil ik kunnen? <br /> Wat wil ik leren?</strong></p>
	    <div class="wheyTXT">{$wheyTXT}</div>
	    <input type="submit" name="btnShowEdit" id="btnShowEdit" value="Pas aan" />
	</form>
    </div>
    {/option:oWhey}
    {option:oEditWhey}
    <form action="{$formAction}" method="post" id="formWhey">
	<p><strong>Wat wil ik kunnen? <br /> Wat wil ik leren?</strong></p>
	<div>
		<!-- Gets replaced with TinyMCE, remember HTML in a textarea should be encoded -->
		<textarea class="mceSimple" id="whey" name="whey" style="width: 80%">{$wheyTXT}</textarea>
		<span class="message error" id="msgWhey">{$msgWhey|htmlentities}</span>
	</div>
	<input type="submit" name="btnEdit" id="btnEdit" value="OK" />
    </form>
    {/option:oEditWhey}
    {option:oOthers}
    <div id="formOthers">
	<form action="{$formAction}" method="post" id="formOthers">
	    <p><strong>Wat wil ik later worden? <br /> Waar droom ik van?</strong></p>
	    <div class="wheyTXT">{$wheyTXTOthers}</div>
	    <input type="submit" name="btnShowEditOthers" id="btnShowEditOthers" value="Pas aan" />
	</form>
    </div>
    {/option:oOthers}
    {option:oEditOthers}
    <form action="{$formAction}" method="post" id="formOthers">
	<p><strong>Wat wil ik later worden? <br /> Waar droom ik van?</strong></p>
	<div>
		<!-- Gets replaced with TinyMCE, remember HTML in a textarea should be encoded -->
		<textarea class="mceSimple" id="wheyOthers" name="wheyOthers" style="width: 80%">{$wheyTXTOthers}</textarea>
		<span class="message error" id="msgWheyOthers">{$msgWheyOthers|htmlentities}</span>
	</div>
	<input type="submit" name="btnEditOthers" id="btnEditOthers" value="OK" />
    </form>
    {/option:oEditOthers}
    
    <div id="blank">
    </div>

    <form action="{$formActionUpload}" method="post" id="formUpload" enctype="multipart/form-data">
	<input type="file" name="upload" id="upload" />
	<input type="submit" name="btnUpload" id="btnUpload" value="Uploaden" />
	<p><span class="message error" id="msgUpload">{$msgUpload}</span></p>
    </form>

    <div id="files">
	<div class="documents">
	    <h3 class="filesTitle">Documenten</h3>
	    <ul class="files clearfix">
		{iteration:iDocuments}
		<li>
		    <a href="{$hrefDoc}" title="open this file">{$nameDoc}</a>
		    <a class="delete" href="{$urlDel}" title="delete this file">Verwijder</a>
		</li>
		{/iteration:iDocuments}
	    </ul>
	</div>
	<div class="pictures">
	    <h3 class="filesTitle">Afbeeldingen</h3>
	    <ul class="files clearfix">
		{iteration:iPictures}
		<li>
		    <a href="{$hrefPic}" title="open this file">{$namePic}</a>
		    <a class="delete" href="{$urlDel}" title="delete this file">Verwijder</a>
		</li>
		{/iteration:iPictures}
	    </ul>
	</div>
	<div class="sounds">
	    <h3 class="filesTitle">Geluiden</h3>
	    <ul class="files clearfix">
		{iteration:iSounds}
		<li>
		    <a href="{$hrefSound}" title="open this file">{$nameSound}</a>
		    <a class="delete" href="{$urlDel}" title="delete this file">Verwijder</a>
		</li>
		{/iteration:iSounds}
	    </ul>
	</div>
	<div class="movies">
	    <h3 class="filesTitle">Filmpjes</h3>
	    <ul class="files clearfix">
		{iteration:iMovies}
		<li>
		    <a href="{$hrefMovie}" title="open this file">{$nameMovie}</a>
		    <a class="delete" href="{$urlDel}" title="delete this file">Verwijder</a>
		</li>
		{/iteration:iMovies}
	    </ul>
	</div>
    </div>