    {option:oDelete}
    <h2>Ben je zeker?</h2>

    <form action="{$formAction}" method="post" id="deleteForm">
	<p>Ben je zeker dat je dit bestand wilt verwijderen?</p>
	<p><a href="openfile.php?file={$urlFile}">{$urlFile}</a></p>
	<p><input type="submit" name="btnDelete" id="btnDelete" value="verwijder dit bestand" /></p>
    </form>

    <h2>Keer terug</h2>
    <p>Als je wilt terugkeren naar je TrotsStick, klik dan op <a href="index.php">deze link</a>.</p>
    {/option:oDelete}

    {option:oDeleted}
    <h2>Je bestand werd verwijderd.</h2>

    <h2>Keer terug</h2>
    <p>Als je wilt terugkeren naar je TrotsStick, klik dan op <a href="index.php">deze link</a>.</p>
    {/option:oDeleted}

    {option:oNoFileSelected}
    <h2>Geen bestand geselecteerd</h2>
    <p>Je hebt blijkbaar geen bestand geselecteerd. Je kan dus ook niets verwijderen.</p>

    <h2>Keer terug</h2>
    <p>Als je wilt terugkeren naar je TrotsStick, klik dan op <a href="index.php">deze link</a>.</p>
    {/option:oNoFileSelected}