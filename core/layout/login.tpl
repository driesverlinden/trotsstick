    {option:oAlreadyConfig}
    <h3>Aanmelden</h3>

    <form action="{$formAction}" method="post" id="loginForm">
	<dl class="clearfix">
	    <dt><label for="login">Naam:</label></dt>
	    <dd>
		<input type="text" name="login" id="login" maxlength="20" size="20" value="{$login|htmlentities}" />
		<span class="message error" id="msglogin">{$msgLogin|htmlentities}</span>
	    </dd>

	    <dt><label for="pw">Wachtwoord:</label></dt>
	    <dd>
		<input type="password" name="pw" id="pw" maxlength="20" size="20" value="{$pw|htmlentities}" />
		<span class="message error" id="msgpw">{$msgPw|htmlentities}</span>
	    </dd>

	    <dt><label for="autologin">&nbsp;</label></dt>
	    <dd>
		<label for="autologin"><input type="checkbox" class="option" name="autologin" id="autologin" {option:oAutoLogin} checked="true" {/option:oAutoLogin} /> Onthou mijn gegevens</label>
	    </dd>

	    <dt class="full clearfix buttonrow">
		<input type="submit" id="btnOk" name="btnOk" value="Aanmelden" />
	    </dt>
	</dl>
    </form>
    {/option:oAlreadyConfig}

    {option:oCreateStick}
    <h2>Configureer je TrotsStick</h2>

    <p id="signup">Welkom op je TrotsStick! Je hebt je stick blijkbaar nog niet geconfigureerd.</p>
    <p><a href="register.php">Stel je TrotsStick in</a>.</p>
    {/option:oCreateStick}

    <p class="about"><a href="about.php">De TrotsStick</a></p>
			