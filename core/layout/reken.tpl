    <h3>Rekenknap</h3>

    <ul>
      <li><strong>Ik hou van cijferpuzzels</strong> en nog onbekende rekenopdrachten.</li>
      <li>Ik geef blijk van inzicht in getalsverhoudingen <br />
      (bijvoorbeeld bij de tafels: 2 x 6 = 12 &ldquo;Oh dan is 4 x 6 het dubbele!&rdquo;)</li>
      <li>Ik leg materiaal/eten (en dergelijke) gestructureerd neer en pak het systematisch op.</li>
      <li>Ik heb de neiging om een rekensom/probleemstelling op verschillende manieren te willen uitrekenen/benaderen, bedenk (nieuwe), originele en vaak logische oplossingen.</li>
      <li>Ik hou van meten met meetinstrumenten of met stokjes, vingers, draadjes.</li>
      <li>Ik heb graag duidelijkheid en logica, ik hou van gestructureerd werken.</li>
      <li>Ik heb iets met &ldquo;tijd&rdquo;, geschiedenisfeiten, jaartallen.</li>
      <li>Ik zoek graag uit hoe ik iets kan maken, bijvoorbeeld met lego of ander constructiemateriaal.</li>
      <li>Ik leg graag verbanden, ik gebruik eventueel schema&rsquo;s om die te verduidelijken.</li>
      <li>Ik hou ervan probleemoplossend bezig te zijn en dan de zaak op een rijtje te zetten, het zit zus en zo in elkaar.</li>
      <li>Ik wil weten hoe iets werkt.</li>
      <li>Ik hanteer &ldquo;zwart - wit&rdquo; redeneringen.</li>
      <li>Ik denk kritisch.</li>
      <li>Ik ben vaak zuinig met woorden.</li>
      <li>Ik hou van spelletjes als schaken, dammen en kaarten.</li>
    </ul>
    <p><em>Bron: Vrij naar &ldquo;De kracht van meervoudige intelligentie&rdquo; (www.bazalt.nl)</em></p>
    <p><a href="waarknap.php">Terug</a></p>
    
  