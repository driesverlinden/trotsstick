    {option:oCreateStick}
    <h2>Maak je TrotsStick aan</h2>

    <form action="{$formAction|htmlentities}" method="post" id="registerForm">
	<p class="message">Velden waar een <span class="error">*</span> naast staat zijn verplicht</p>
	<dl class="clearfix">
	    <dt><label for="login">Mijn naam:</label></dt>
	    <dd>
			<input type="text" name="login" id="login" maxlength="20" size="20" value="{$login|htmlentities}" />
			<span class="message error" id="msglogin">{$msgLogin|htmlentities}</span>
	    </dd>
	    <dt><label for="pw">Een wachtwoord:</label></dt>
	    <dd>
			<input type="password" name="pw" id="pw" maxlength="20" size="20" value="{$pw|htmlentities}" />
			<span class="message error" id="msgpw">{$msgPw|htmlentities}</span>
	    </dd>
	    <dt>&nbsp;</dt>
	    <dd>
	    	<input type="submit" name="btnRegister" id="btnRegister" value="maak aan" />
	    	<span class="message error" id="msgError">{$msgError|htmlentities}</span>
	    </dd>
	</dl>
    </form>
    {/option:oCreateStick}

    {option:oAlreadyConfig}
    <h2>Oei, deze TrotsStick is al van iemand!</h2>

    <p>Blijkbaar is deze TrotsStick al van iemand. Je kan dus geen nieuwe gebruiker meer aanmaken.</p>
    {/option:oAlreadyConfig}