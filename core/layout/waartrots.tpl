    <h2 id="wheyproud"><a href="index.php">Waar ben ik trots op?</a></h2>
    <div class="super">
	{option:oNoSuper}
	<h3>Hier ben ik nu supertrots op!</h3>
	<ul>
	    <li><img src="{$superPic}" alt="" height="250" /></li>
	</ul>
	<form action="{$formAction}" method="post" id="formSuper" enctype="multipart/form-data">
	    <input type="file" name="super" id="super" />
	    <input type="submit" name="btnSuper" id="btnSuper" value="Upload" />
	    <span class="message error" id="msgSuper">{$msgSuper}</span>
	</form>
	{/option:oNoSuper}
	{option:oSuper}
	<h3>Hier ben ik supertrots op!</h3>
	<ul>
	    <li>
		<img src="{$superPic}" alt="" height="310" />
		<a class="deleteSuper" href="{$urlDel}" title="delete super" onclick="return confirm('Ben je zeker dat je deze foto wilt verwijderen?');">Verwijder</a>
	    </li>
	</ul>
	{/option:oSuper}
   </div>
    <div id="text">
	<ul class="clearfix">
	    <li>Trotsmomenten uit de klas</li>
	    <li>Trotsmomenten van thuis</li>
	    <li>Trotsmomenten uit mijn hobby</li>
	    <li>Trotsmomenten ...</li>
	</ul>
    </div>

    {option:oWhey}
    <div id="formWhey">
	<form action="{$formAction}" method="post" id="formWhey">
	    <p><strong>Hier schrijf ik waar ik trots op ben.</strong></p>
	    <div class="wheyTXT">{$wheyTXT}</div>
	    <input type="submit" name="btnShowEdit" id="btnShowEdit" value="Pas aan" />
	</form>
    </div>
    {/option:oWhey}
    {option:oEditWhey}
	<form action="{$formAction}" method="post" id="formWhey">
	    <p><strong>Hier schrijf ik waar ik trots op ben.</strong></p>
	    <div>
		    <!-- Gets replaced with TinyMCE, remember HTML in a textarea should be encoded -->
		    <textarea class="mceSimple" id="whey" name="whey" style="width: 80%">{$wheyTXT}</textarea>
		    <span class="message error" id="msgWhey">{$msgWhey|htmlentities}</span>
	    </div>
	    <input type="submit" name="btnEdit" id="btnEdit" value="OK" />
	</form>
    {/option:oEditWhey}

    <div id="blank">
    </div>

    <form action="{$formActionUpload}" method="post" id="formUpload" enctype="multipart/form-data">
	<input type="file" name="upload" id="upload" />
	<input type="submit" name="btnUpload" id="btnUpload" value="Uploaden" />
	<p><span class="message error" id="msgUpload">{$msgUpload}</span></p>
    </form>

    <div id="files">
	<div class="documents">
	    <h3 class="filesTitle">Documenten</h3>
	    <ul class="files clearfix">
		{iteration:iDocuments}
		<li>
		    <a href="{$hrefDoc}">{$nameDoc}</a>
		    <a class="open" href="{$urlOpen}" title="open this file"></a>
		    <a class="delete" href="{$urlDel}" title="delete this file">Verwijder</a>
		</li>
		{/iteration:iDocuments}
	    </ul>
	</div>
	<div class="pictures">
	    <h3 class="filesTitle">Afbeeldingen</h3>
	    <ul class="files clearfix">
		{iteration:iPictures}
		<li>
		    <a href="{$hrefPic}">{$namePic}</a>
		    <a class="open" href="{$urlOpen}" title="open this file"></a>
		    <a class="delete" href="{$urlDel}" title="delete this file">Verwijder</a>
		</li>
		{/iteration:iPictures}
	    </ul>
	</div>
	<div class="sounds">
	    <h3 class="filesTitle">Geluiden</h3>
	    <ul class="files clearfix">
		{iteration:iSounds}
		<li>
		    <a href="{$hrefSound}">{$nameSound}</a>
		    <a class="open" href="{$urlOpen}" title="open this file"></a>
		    <a class="delete" href="{$urlDel}" title="delete this file">Verwijder</a>
		</li>
		{/iteration:iSounds}
	    </ul>
	</div>
	<div class="movies">
	    <h3 class="filesTitle">Filmpjes</h3>
	    <ul class="files clearfix">
		{iteration:iMovies}
		<li>
		    <a href="{$hrefMovie}">{$nameMovie}</a>
		    <a class="open" href="{$urlOpen}" title="open this file"></a>
		    <a class="delete" href="{$urlDel}" title="delete this file">Verwijder</a>
		</li>
		{/iteration:iMovies}
	    </ul>
	</div>
    </div>