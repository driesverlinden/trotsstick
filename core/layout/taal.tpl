    <h3>Taalknap</h3>

    <ul>
      <li><strong>Ik vertel graag</strong> in geuren en in kleuren, en ik houd ervan te praten.</li>
      <li>Ik geniet van &ldquo;expressief taalgebruik&rdquo; (woordherhalingen, rijmpjes, melodietjes).</li>
      <li>Ik lees graag (krant, boek, stripverhaal, opschriften).</li>
      <li>Ik maak woordgrapjes of doe graag taalspelletjes.</li>
      <li>Ik kan spelen met mijn stem (stemmetjes maken).</li>
      <li>Ik pik graag nieuwe woorden en begrippen op, en gebruik ze tijdens het spreken.</li>
      <li>Ik hou ervan voorgelezen te worden en kan goed voorlezen.</li>
      <li>Ik schrijf graag (verhalen, informatieve teksten, woorden bij een tekening).</li>
      <li>Ik rijm graag of schrijf gedichten.</li>
      <li>Ik luister graag naar iets dat verteld wordt, en ik ben betrokken bij gesprekken.</li>
      <li>Ik heb interesse voor de juiste spelling.</li>
      <li>Ik heb een grote woordenschat en vraag vaak om uitleg: &ldquo;Wat betekent dit, wat is dat?&rdquo;</li>
      <li>Ik kan anderen verbaal corrigeren of overtuigen.</li>
    </ul>
    <p><em>Bron: Vrij naar &ldquo;De kracht van meervoudige intelligentie&rdquo; (www.bazalt.nl)</em></p>
    <p><a href="waarknap.php">Terug</a></p>
    
  