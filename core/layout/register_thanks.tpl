    <h2>Je TrotsStick werd aangemaakt!</h2>

    <p>Beste TrotsStick-gebruiker</p>
    <p>Jij gaat nu de grote overstap maken van de Trotsdoos naar de TrotsStick. Proficiat! Ik ben nu al trots op jou!</p>
    <p>
	Ik zou de Trotsdoos toch nog niet te ver weg zetten, want ik ben er zeker van dat de TrotsStick een plaatsje zal vinden in de Trotsdoos. Sommige voorwerpen bewaar je beter in je Trotsdoos, andere ervaringen of talenten kun je dan weer op je TrotsStick bewaren.<br />
	De TrotsStick geeft je de kans om jezelf voor te stellen, om je talenten te verzamelen, om te laten zien wat je al kan, om te beslissen waaraan je wil of moet werken.
    </p>
    <p>Alleen jij kan je TrotsStick openen met een wachtwoord. Misschien wil je je TrotsStick ook wel eens laten zien aan een vriend of een vriendin, aan je ouders, aan je leraar, aan iemand van je familie ...</p>
    <p>Vanaf nu ben jij de trotse eigenaar van je persoonlijke TrotsStick. Draag er zorg voor en maak regelmatig wat tijd vrij om hem aan te vullen.</p>
    <form action="register_thanks.php" method="get">
	<p><input type="submit" id="btnBack" name="btnBack" value="Open jouw TrotsStick" /></p>
    </form>