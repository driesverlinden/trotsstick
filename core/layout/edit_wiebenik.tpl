    <h2>Wijzig je persoonlijke gegevens</h2>

    <form action="{$formAction}" method="post" id="formUploadWho" enctype="multipart/form-data">
	<dl class="clearfix">
	    <dt><label for="name">Naam:</label></dt>
	    <dd>
		<input type="text" name="name" id="name" maxlength="20" size="20" value="{$name|htmlentities}" />
		<span class="message error" id="msgName">{$msgName|htmlentities}</span>
	    </dd>

	    <dt><label for="street">Straat:</label></dt>
	    <dd>
		<input type="text" name="street" id="street" maxlength="20" size="20" value="{$street|htmlentities}" />
		<span class="message error" id="msgStreet">{$msgStreet|htmlentities}</span>
	    </dd>

	    <dt><label for="street">Gemeente:</label></dt>
	    <dd>
		<input type="text" name="city" id="city" maxlength="20" size="20" value="{$city|htmlentities}" />
		<span class="message error" id="msgCity">{$msgCity|htmlentities}</span>
	    </dd>

	    <dt><label for="phone">Telefoonnummer:</label></dt>
	    <dd>
		<input type="text" name="phone" id="phone" maxlength="20" size="20" value="{$phone|htmlentities}" />
		<span class="message error" id="msgPhone">{$msgPhone|htmlentities}</span>
	    </dd>

	    <dt><label for="email">E-mailadres:</label></dt>
	    <dd>
		<input type="text" name="email" id="email" size="30" value="{$email|htmlentities}" />
		<span class="message error" id="msgEmail">{$msgEmail|htmlentities}</span>
	    </dd>

	    <dt><label for="you">Mijn foto:</label></dt>
	    <dd>
		<input type="file" name="you" id="you" size=35 />
		<span class="message error" id="msgYou">{$msgYou}</span>
	    </dd>

	    <dt><label for="family">Foto van mijn gezin:</label></dt>
	    <dd>
		<input type="file" name="family" id="family" size=35 />
		<span class="message error" id="msgFamily">{$msgFamily}</span>
	    </dd>

	    <dt><label for="family">2de foto van mijn gezin:</label></dt>
	    <dd>
		<input type="file" name="family2" id="family2" size=35 />
		<span class="message error" id="msgFamily2">{$msgFamily2}</span>
	    </dd>

	    <dt><label for="hobby">Mijn hobby's:</label></dt>
	    <dd>
		<div>
		    <!-- Gets replaced with TinyMCE, remember HTML in a textarea should be encoded -->
		    <textarea class="mceSimple" id="hobby" name="hobby" style="width: 80%">{$hobby}</textarea>
		    <span class="message error" id="msgHobby">{$msgHobby|htmlentities}</span>
		</div>
	    </dd>
	    <dt><label for="picHobby">Foto van mijn hobby:</label></dt>
	    <dd>
		<input type="file" name="picHobby" id="picHobby" />
		<span class="message error" id="msgPicHobby">{$msgPicHobby}</span>
	    </dd>

	    <dt><label for="talent">Mijn talenten:</label></dt>
	    <dd>
		<div>
		    <!-- Gets replaced with TinyMCE, remember HTML in a textarea should be encoded -->
		    <textarea class="mceSimple" id="talent" name="talent" style="width: 80%">{$talent}</textarea>
		    <span class="message error" id="msgTalent">{$msgTalent|htmlentities}</span>
		</div>
	    </dd>

	    <dt><label for="picTalent">Foto van mijn talent:</label></dt>
	    <dd>
		<input type="file" name="picTalent" id="picTalent" />
		<span class="message error" id="msgPicTalent">{$msgPicTalent}</span>
	    </dd>

	    <dt class="full clearfix buttonrow">
		<input type="submit" id="btnRegister" name="btnEdit" value="Pas aan" />
		<input type="submit" id="btnCancel" name="btnCancel" value="Annuleer" />
	    </dt>
	</dl>
    </form>