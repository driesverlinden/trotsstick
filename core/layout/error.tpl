    <h2>Oei! Er ging iets verkeerd!</h2>

    <div class="box" id="boxError">
	<p>Deze fout deed zich voor:</p>
	<ul class="errors">
		<li>{$msgErr|htmlentities}</li>
	</ul>
    </div>
    <p class="cancel"><a href="index.php" title="Cancel and go back">ga terug naar je TrotsStick</a></p>