<?php
	/**
	 * Configuration for TrotsStick
	 *
	 * @author	Andries Verlinden <andriesverlinden@gmail.com>
	 * @version	1.9
	 */

	// DB information
	define('DB_HOST', 'localhost');
	define('DB_USER', 'root');
	define('DB_PASS', 'root');
	define('DB_NAME', 'trotsstick');

	//define('DB_PASS', 'usbw');

	// TS information
	define('VERSION', '1.9');
	define('LASTUPDATE', '20 januari 2012');
	define('DRIVE', dirname(__FILE__) . '/../../');