<?php

    /**
     * Guaranteed slashes (if magic_quotes is off, it adds the slashes)
     *
     * @param string $string The string to add the slashes to
     * @return string
     */
    function addPostSlashes($string) {
	    if ((get_magic_quotes_gpc()==1) || (get_magic_quotes_runtime()==1))
		    return $string;
	    else return addslashes($string);
    }

    /**
     * Guaranteed no slashes (if magic_quotes is on, it strips the slashes)
     *
     * @param string $string The string to remove the slashes from
     * @return string
     */
    function stripPostSlashes($string) {
	    if ((get_magic_quotes_gpc()==1) || (get_magic_quotes_runtime()==1))
		    return stripslashes($string);
	    else return $string;
    }

    /**
     * Checks if the string $haystack ends with the string $needle
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    function endsWith($haystack, $needle) {
	    return (substr($haystack, strlen($haystack) - strlen($needle)) == $needle);
    }

    function beginsWith($haystack, $needle) {
	    return (strncmp($string, $search, strlen($search)) == $needle);
    }

    /**
     * Checks if a string $file is a possible .jpg file
     * @param string $file
     * @return bool
     */
    function isJpg($file) {
	    $file = strtolower((string) $file);
	    if (endsWith($file, '.jpg')) return true;
	    if (endsWith($file, '.jpeg')) return true;
	    return false;
    }
	
	/**
     * Checks if a string $file is a possible .pdf file
     * @param string $file
     * @return bool
     */
    function isPdf($file) {
	    $file = strtolower((string) $file);
	    if (endsWith($file, '.pdf')) return true;
	    return false;
    }
    
    /**
     * Checks if a collection exists
     * @param string $baseDir
     * @param string $collectionName
     * @return boolean
     */
    function collectionExists($baseDir, $collectionName) {
	    return (file_exists($baseDir . '/' . $collectionName) && is_dir($baseDir . '/' . $collectionName));
    }


    /**
     * Creates a collection
     * @param string $baseDir
     * @param string $collectionName
     */
    function collectionCreate($baseDir, $collectionName) {
	    @mkdir($baseDir . '/' . $collectionName) or showError('Could not create collection ', $baseDir . '/' . $collectionName);
    }


    /**
     * Renames a collection
     * @param string $baseDir
     * @param string $oldName
     * @param string $newName
     */
    function collectionRename($baseDir, $oldName, $newName) {
	    if (@rename($baseDir . '/' . $oldName, $baseDir . '/'. $newName) !== true) {
		    showError('Could not rename collection ' , $oldname .' to ' . $newName);
	    }
    }

    /**
     * Detects if a string contains a possible dangerous path (viz. '../' or './')
     * @param string $path
     * @return
     */
     
    function isPossiblyADangerousPath($path) {

	    // enforce a / at the end
	    if (!endsWith($path, '/'))	$path = $path . '/';

	    // path is ok if it doens't refer to the current dir, or one (or more) dirs up
	    return ((strstr($path, './') !== false) || (strstr($path, '../') !== false));

    }
    
    
    /**
     * Checks if a collection is empty or not
     * @param string $baseDir
     * @param string $collectionName
     * @return boolean
     */
    function collectionIsEmpty($baseDir, $collectionName) {

	    // array on which we'll store the children of the given dir
	    $children = array();

	    // build colldirpath
	    $myCollDir = $baseDir . '/' . $collectionName;

	    // open dir
	    $dp = opendir($myCollDir) or showError('error reading ', $myCollDir);

	    // loop all children of the dir and add'm to an array
	    while (($file = readdir($dp)) !== false) {
		    if ($file == '.') continue;
		    if ($file == '..') continue;
		    $children[] = $file;
	    }

	    // close pointer
	    closedir($dp);

	    // we've got children? no -> empty // yes -> not empty
	    if (sizeof($children) == 0)
		    return true;
	    else
		    return false;
    }


    function collectionDelete($baseDir, $collectionName) {
	    if (@rmdir($baseDir . '/' . $collectionName) !== true) {
		    showError('Could not delete collection ', $collectionName);
	    }
    }

 
    /*function saveUser($login, $pw, $question, $answer) {

		$db = new TrotsStick(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$db->connect();

		$values = array('name' => $login, 'password' => $pw, 'question' => $question, 'answer' => $answer);

		$db->insert('users', $values);
    }*/
    
    function saveUser($login, $pw) {

		$db = new TrotsStick(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$db->connect();

		$values = array('name' => $login, 'password' => $pw);

		$db->insert('users', $values);
    }

    function saveUserRecords($name, $street, $city, $phone, $email, $hobby, $talent) {

	$db = new TrotsStick(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	$db->connect();
	$values = array('name' => $name, 'address' => $street, 'city' => $city, 'phone' => $phone, 'mail' => $email, 'hobby' => stripPostSlashes($hobby), 'talent' => stripPostSlashes($talent));
	$amount = $db->retrieveOne('SELECT COUNT(*) FROM userrecords');
	
	if ($amount['COUNT(*)'] != 0) {
		$db->update('userrecords', $values, 'id=0');
	} else { 
		// eerste keer
		$db->insert('userrecords', $values);
	}

    }

    function makeDirMember($baseDir, $user) {
	    @mkdir($baseDir . '/' . $user) or showError('Could not create user directory ', $baseDir . '/' . $user);
	    @mkdir($baseDir . '/' . $user . '/red/') or showError ('Kan map niet aanmaken', $baseDir . '/' . $user . '/red');
	    @mkdir($baseDir . '/' . $user . '/yellow/') or showError ('Kan map niet aanmaken', $baseDir . '/' . $user . '/yellow');
	    @mkdir($baseDir . '/' . $user . '/green/') or showError ('Kan map niet aanmaken', $baseDir . '/' . $user . '/green');
	    @mkdir($baseDir . '/' . $user . '/orange/') or showError ('Kan map niet aanmaken', $baseDir . '/' . $user . '/orange');
    }

    function isExAcceptable($file) {
	if (isPic($file)) { return true; }
	if (isDoc($file)) { return true; }
	if (isMovie($file)) { return true; }
	if (isSound($file)) { return true; }
	return false;
    }

    function isSizeAcceptable($size) {
	    if ((int)$size < 32505856) { return true; }
	    return false;
    }

    function isLengthAcceptable($file) {
	if ((int)strlen($file) < 43) { return true; }
	return false;
    }

    function isPic($file) {
	    $file = strtolower((string) $file);
	    if (endsWith($file, '.jpg')) return true;
	    if (endsWith($file, '.jpeg')) return true;
	    if (endsWith($file, '.png')) return true;
	    if (endsWith($file, '.gif')) return true;
	    return false;
    }

    function isDoc($file) {
	    $file = strtolower((string) $file);
	    if (endsWith($file, '.doc')) return true;
	    if (endsWith($file, '.docx')) return true;
	    if (endsWith($file, '.xls')) return true;
	    if (endsWith($file, '.xlsx')) return true;
	    if (endsWith($file, '.pdf')) return true;
	    if (endsWith($file, '.ppt')) return true;
	    if (endsWith($file, '.pptx')) return true;
	    return false;
    }

    function isMovie($file) {
	    $file = strtolower((string) $file);
	    if (endsWith($file, '.mp4')) return true;
	    if (endsWith($file, '.wmv')) return true;
	    if (endsWith($file, '.mov')) return true;
	    if (endsWith($file, '.avi')) return true;
	    return false;
    }

    function isSound($file) {
	    $file = strtolower((string) $file);
	    if (endsWith($file, '.mp3')) return true;
	    if (endsWith($file, '.wav')) return true;
	    return false;
    }



    /**
     * Checks if a user exists or not
     * @param string $login
     * @return bool
     */
    function userExists($login) {
	    $usersFile = dirname(__FILE__) . '/members/users.csv';
	    if (!file_exists($usersFile)) return false;
	    $users = file($usersFile);
	    foreach ($users as $user) {
		    $arrUserData = explode(';', $user);
		    if ($arrUserData[0] === (string) $login) return true;
	    }
	    return false;
    }

    /**
     * Redirects to the error handling page
     * @param string $type
     * @param object $dbhandler
     * @return void
     */
    function showError($type, $file) {

	// The referrerd page will show a proper error based on the $_GET parameters
	header('location: error.php?type=' . $type . '&detail=' . $file);
	exit();

    }

    // Security
    
    function checkSecureUser($username, $password) {
    	if ($username == $password) {
    		return false;
	}
	return true;
    }

    function checkSecurePw($password) {
	if (strlen($password) < 6) {
	    return false;
	} else {
	    return true;
	}
    }

    function deleteTrashes($directory) {
	$handle = opendir($directory);
	$array = array();

	while (false !== ($file = readdir($handle))) {
	    if ($file != "." && $file != "..") {
		if(is_dir($directory.$file)) {
		    if(!@rmdir($directory.$file)) { // Empty directory? Remove it
			@unlink($directory.$file.'/'); // Not empty? Delete the files inside it
		    }
		} else {
		    @unlink($directory.$file);
		}
	    }
	}
	closedir($handle);
	@rmdir($directory);
    }