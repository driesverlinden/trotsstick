<?php
	/**
	 * Register page for TrotsStick
	 *
	 * @author	Andries Verlinden <andriesverlinden@gmail.com>
	 * @version	1.6
	 */


	/**
	 * Includes
	 * ----------------------------------------------------------------
	 */

	    // config & functions
	    require_once './core/includes/config.php';
	    require_once './core/includes/functions.php';

	    // needed classes
	    require_once './core/includes/classes/database.php';
	    require_once './core/includes/classes/template.php';


	/**
	 * Database connection
	 * ----------------------------------------------------------------
	 */

	    $db = new TrotsStick(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	    $db->connect();

	/**
	 * Initial Values
	 * ----------------------------------------------------------------
	 */

	    // other form fields
	    $login		= (isset($_POST['login']) ? ((string) $_POST['login']) : '');
	    $pw			= (isset($_POST['pw']) ? ((string) $_POST['pw']) : '');
	    /*$question	= (isset($_POST['question']) ? ((string) $_POST['question']) : '');
	    $answer		= (isset($_POST['answer']) ? ((string) $_POST['answer']) : '');*/

	    $myBaseDir = dirname(__FILE__) . '/members/';

	    // database info
	    $users = $db->retrieveOne('SELECT * FROM users');
 
	    // already configured
	    $alreadyConfig = false;

	    // clear error messages
	    $msgLogin 		= '*';
	    $msgPw    		= '*';
	    $msgError 		= '';
	    /*$msgQuestion 	= '*';
	    $msgAnswer		= '*';*/


	/**
	 * Check if the TrotsStick isn't configured already
	 * (only one user is allowed)
	 * ----------------------------------------------------------------
	 */
		
	    if (count($users) > 0) {
		$alreadyConfig = true;
	    }
	    

	/**
	 * Start session and check if we are logged in
	 * ----------------------------------------------------------------
	 */

	    // start a session
	    session_start();

	    // are we logged in or not
	    $loggedIn = isset($_SESSION['loggedin']) ? $_SESSION['loggedin'] : false;

	    if ($loggedIn === true) {
		header('location: index.php');
		exit();
	    }

	    
	/**
	 * Handle action 'btnCancel' (user pressed cancel button)
	 * ----------------------------------------------------------------
	 */

	    if (isset($_POST['btnCancel'])) {
		header('Location: login.php');
		exit(0);
	    }


	/**
	 * Handle action 'btnRegister' (user pressed register button)
	 * ----------------------------------------------------------------
	 */
		
	    if (isset($_POST['btnRegister'])) {
		// allOk?
		$allOk = true;

		// regexes we'll need
		$rexLogin		= '/^[a-zA-Z0-9 ]+$/';		// any word character (1 or more)
		$rexPw			= '/^[^<"\'>;]+$/';			// no ^ < " ' > or ;
		/*$rexQuestion	= '/^[a-zA-Z0-9-? ]+$/';	// any word character (1 or more)
		$rexAnswer		= '/^[a-zA-Z0-9 ]+$/';		// any word character (1 or more)*/

		// check login
		    // check if user doesn't exist
		    if (userExists($login)) {
			$msgLogin = 'De gekozen gebruikersnaam bestaat al!';
			$allOk = false;
		    }

		    // Emtpy field?
		    if ($login == '') {
			$msgLogin = 'Kies een gebruikersnaam';
			$allOk = false;
		    }

		    // Not an empty field ...
		    else {
			// check syntax (regex)
			if (!preg_match($rexLogin, $login)) {
			    $msgLogin = 'Je mag enkel nummers, letters en liggende streepjes gebruiken in je naam.';
			    $allOk = false;
			}
		    }

		// check password
		    // Emtpy field?
		    if ($pw === '') {
			$msgPw = 'Kies een wachtwoord';
			$allOk = false;
		    }

		    // Not an empty field ...
		    else {
			// check syntax (regex)
				if (!preg_match($rexPw, $pw)) {
			  	  $msgPw = 'In je wachtwoord mogen deze tekens niet staan < > ; " or \'.';
			   	 $allOk = false;
				}
		    }
		    
		// check security user
			
			if (!checkSecureUser($login, $pw)) {
			    $allOk = false;
			    $msgError = 'Je gebruikersnaam en wachtwoord zijn niet veilig genoeg! Deze mogen bijvoorbeeld niet gelijk zijn.';
			}
			
			if (!checkSecurePw($pw)) {
			    $allOk = false;
			    $msgError = 'Geef een nieuw wachtwoord op. Een wachtwoord telt minstens zes tekens.';
			}

		// check question
			// Emtpy field?
		/*    if ($question == '') {
				$msgQuestion = 'Gelieve een vraag op te geven.';
				$allOk = false;
		    }

		    // Not an empty field ...
		    else {
				// check syntax (regex)
				if (!preg_match($rexQuestion, $question)) {
				    $msgQuestion = 'Je mag enkel nummers, letters en liggende streepjes gebruiken in je vraag.';
				    $allOk = false;
				}
			}

		// check answer
			// Emtpy field?
		    if ($answer == '') {
				$msgQuestion = 'Gelieve een antwoord op te geven.';
				$allOk = false;
		    }

		    // Not an empty field ...
		    else {
				// check syntax (regex)
				if (!preg_match($rexAnswer, $answer)) {
				    $msgAnswer = 'Je mag enkel nummers, letters en liggende streepjes gebruiken in je antwoord.';
				    $allOk = false;
				}
			}
                */
		// process result
		    // all is good!
		    if ($allOk === true) {

			// save user
			saveUser($login, $pw);

			// make directories
			makeDirMember($myBaseDir, $login);

			$_SESSION['loggedin'] = true;
			$_SESSION['login'] = $login;

			// say thank you
			header('Location: register_thanks.php');
			exit(0);
		    }
	    }


	/**
	 * No action to handle: show our page itself
	 * -------------------------------------------------------------
	 */

	    // Main Layout

		// load main layout into a template
		$mainTpl = new Template('./core/layout/layout.tpl');

		// asisgn vars in our main layout tpl
		$mainTpl->assign('pageTitle', 	'TrotsStick - Maak je TrotsStick aan');
		$mainTpl->assign('pageMeta', 	'');
		$mainTpl->assign('pageCss',	'');
		$mainTpl->assign('pageJs',	'<script type="text/javascript" src="core/js/register.js"></script>');
		$mainTpl->assign('pageH2',	'Maak je TrotsStick aan');

	    // Page specific template

		// new template
		$pageTpl = new Template('./core/layout/register.tpl');

		// assign variables of the form
		$pageTpl->assign('formAction', 	$_SERVER['PHP_SELF']);

		// assign login and pw
		$pageTpl->assign('login', 		$login);
		$pageTpl->assign('pw', 			$pw);
		/*$pageTpl->assign('question',	$question);
		$pageTpl->assign('answer',		$answer);*/

		// assign error messages
		$pageTpl->assign('msgPw',		$msgPw);
		$pageTpl->assign('msgLogin',	$msgLogin);
		$pageTpl->assign('msgError',	$msgError);
		/*$pageTpl->assign('msgQuestion',	$msgQuestion);
		$pageTpl->assign('msgAnswer',	$msgAnswer);*/

		// check if TrotsStick isn't configured already
		if ($alreadyConfig == true) {
		    $pageTpl->assignOption('oAlreadyConfig');
		} else {
		    $pageTpl->assignOption('oCreateStick');
		}

	    // Parse page specific layout into main layout
		$mainTpl->assign('pageContent', $pageTpl->getContent());

	    // Output our main layout
		$mainTpl->display();


//EOF