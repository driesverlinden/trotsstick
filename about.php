<?php
	/**
	 * About page for TrotsStick
	 *
	 * @author	Andries Verlinden <andriesverlinden@gmail.com>
	 * @version	1.8
	 */

        /**
	 * Includes
	 * ----------------------------------------------------------------
	 */

	    // config & functions
	    require_once './core/includes/config.php';
	    require_once './core/includes/functions.php';

	    // needed classes
	    require_once './core/includes/classes/template.php';
	    

	/**
	 * Initial Values
	 * ----------------------------------------------------------------
	 */

	    // start session (starts a new one, or continues the already started one)
	    session_start();

	    // check if we are logged in or not
	    $loggedIn = isset($_SESSION['loggedin']) ? $_SESSION['loggedin'] : false;


	/**
	 * Get drive info
	 * ----------------------------------------------------------------
	 */

	    // total disk space
	    $total = round(disk_total_space(DRIVE)/1024/1024/1024,2);

	    $free = round(disk_free_space(DRIVE)/1024/1024/1024,2);
	    $used = $total - $free;


	/**
	 * btnBack : goto TrotsStick
	 * ----------------------------------------------------------------
	 */
		
	    if(isset($_GET['btnBack'])) {
		header('location:index.php');
		exit(0);
	    }

	    
        /**
	 * No action to handle: show our page itself
	 * ----------------------------------------------------------------
	 */

	    // Main Layout

		// load main layout into a template
		$mainTpl = new Template('./core/layout/layout.tpl');

		// asisgn vars in our main layout tpl
		$mainTpl->assign('pageTitle',	'TrotsStick - About');
		$mainTpl->assign('pageMeta',	'');
		$mainTpl->assign('pageCss',	'div.framework { width: 900px; }');
		$mainTpl->assign('pageJs',	'');
		$mainTpl->assign('pageH2',	'About');

		// show logged in user
		if (($loggedIn == true)) {
		    $mainTpl->assignOption('oLoggedIn');
		    $mainTpl->assign('login', $_SESSION['login']);
		}

		// assign menu active state
		$mainTpl->assignOption('oNavAbout');

	    // Page specific template

		// make new template
		$pageTpl = new Template('./core/layout/about.tpl');

		// running version
		$pageTpl->assign('version', VERSION);
		$pageTpl->assign('lastUpdate', LASTUPDATE);

		// disk info
		$pageTpl->assign('free', $free);
		$pageTpl->assign('used', $used);
		$pageTpl->assign('total', $total);

	    // Parse page specific layout into main layout
	    $mainTpl->assign('pageContent', $pageTpl->getContent());

	    // Output our main layout
	    $mainTpl->display();

//EOF
?>