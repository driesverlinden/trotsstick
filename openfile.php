<?php
	/**
	 * Open een bestand page for TrotsStick
	 *
	 * @author	Andries Verlinden <andriesverlinden@gmail.com>
	 * @version	1.6
	 */


        /**
	 * Includes
	 * ----------------------------------------------------------------
	 */

	    // config & functions
	    require_once './core/includes/config.php';
	    require_once './core/includes/functions.php';

	    // needed classes
	    require_once './core/includes/classes/template.php';

        /**
	 * Initial Values
	 * ----------------------------------------------------------------
	 */

	    // start session (starts a new one, or continues the already started one)
	    session_start();

	    // check if we are logged in or not
	    $loggedIn = isset($_SESSION['loggedin']) ? $_SESSION['loggedin'] : false;

	    if ($loggedIn === false) {	// not logged in
		header('location: login.php');
		exit();
	    }

	    // member related params
	    $myMemberId = isset($_SESSION['login']) ? $_SESSION['login'] : 'stranger'; // value from login or sth
	    $myBaseDir = dirname(__FILE__) . '/members/'.$myMemberId;	// Path where files are store
	    $myBaseUrl = 'members/'.$myMemberId;	 // Relative URL where images can be found via HTTP req

	    // type of file + file + url
	    $type = isset($_GET['type']) ? $_GET['type'] : '';
	    $file = isset($_GET['file']) ? $_GET['file'] : '';
	    $urlFile = $myBaseUrl . $file;
	    

	/**
	 * Upload edited file
	 * -----------------------------------------------------------------
	 */

	    if (!empty($_FILES['upload'])) {
		$fileToCopy = $_FILES['upload']['tmp_name'];
		$fileName   = $_FILES['upload']['name'];

		$copyTo     = $myBaseDir.'/'.$fileName;
		@move_uploaded_file($fileToCopy, $copyTo) or showError('cantCopy', $fileName);
		header('Location: index.php');
		exit(0);
	    }


	/**
	 * No action to handle: show our page itself
	 * -----------------------------------------------------------------
	 */
	    // Main Layout

		// load main layout into a template
		$mainTpl = new Template('./core/layout/layout.tpl');

		// asisgn vars in our main layout tpl
		$mainTpl->assign('pageTitle', 	'TrotsStick - Open een bestand');
		$mainTpl->assign('pageMeta', 	'');
		$mainTpl->assign('pageCss',		'');
		$mainTpl->assign('pageJs',		'');
		$mainTpl->assign('pageH2',		'Open een bestand');

		// show logged in user
		if (($loggedIn == true)) {
		    $mainTpl->assignOption('oLoggedIn');
		    $mainTpl->assign('login', $_SESSION['login']);
		}

	    // Page specific template

		// new template
		$pageTpl = new Template('./core/layout/openfile.tpl');

		// file selected ?
		if ($file != '') {
		    $pageTpl->assignOption('oOpen');

		    // which type of file
		    if ($type == 'doc') {
			$pageTpl->assignOption('oDoc');
			$pageTpl->assign('file', $file);
		    }
		    else if ($type == 'pic') {
			$pageTpl->assignOption('oPic');
			$pageTpl->assign('file', $file);
		    }
		    else if ($type == 'mov') {
			$pageTpl->assignOption('oMov');
			$pageTpl->assign('file', $file);
		    }
		    else if ($type == 'sou') {
			$pageTpl->assignOption('oSou');
			$pageTpl->assign('file', $file);
		    }
		    else {
			$pageTpl->assignOption('oFile');
			$pageTpl->assign('file', $file);
		    }

		    // urlFile
		    $pageTpl->assign('urlFile', $urlFile);
		} else {
		    $pageTpl->assignOption('oNoFileSelected');
		}

		

	    // Parse page specific layout into main layout
		$mainTpl->assign('pageContent', $pageTpl->getContent());

	    // Output our main layout
		$mainTpl->display();

		
//EOF
?>