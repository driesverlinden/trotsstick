<?php
	/**
	 * Register danku page for TrotsStick
	 *
	 * @author	Andries Verlinden <andriesverlinden@gmail.com>
	 * @version	1.8
	 */


	/**
	 * Includes
	 * ----------------------------------------------------------------
	 */

	    // config & functions
	    require_once './core/includes/config.php';
	    require_once './core/includes/functions.php';

	    // needed classes
	    require_once './core/includes/classes/template.php';


	/**
	 * Initial Values
	 * ----------------------------------------------------------------
	 */

	    session_start(); // start session (starts a new one, or continues the already started one)
	    $loggedIn = isset($_SESSION['loggedin']) ? $_SESSION['loggedin'] : false; // are we logged in or not

	    if ($loggedIn === false) {	// already logged in!
		header('location: login.php');
		exit();
	    }


	/**
	 * btnBack : goto TrotsStick
	 * ----------------------------------------------------------------
	 */

	    if(isset($_GET['btnBack'])) {
		header('location:index.php');
		exit(0);
	    }
	    
		
	/**
	 * No action to handle: show our page itself
	 * -----------------------------------------------------------------
	 */
		// Main Layout

		    // load main layout into a template
		    $mainTpl = new Template('./core/layout/layout.tpl');

		    // asisgn vars in our main layout tpl
		    $mainTpl->assign('pageTitle',   'TrotsStick - Configuratie geslaagd');
		    $mainTpl->assign('pageMeta',    '');
		    $mainTpl->assign('pageCss',	    'h2 { font-size: 16px;}');
		    $mainTpl->assign('pageJs',	    '');
		    $mainTpl->assign('pageH2',	    'Configuratie geslaagd');

		// Page specific template

		    // new template
		    $pageTpl = new Template('./core/layout/register_thanks.tpl');

		    // assign variables of the form
		    $pageTpl->assign('formAction', 	$_SERVER['PHP_SELF']);

		// Parse page specific layout into main layout
		    $mainTpl->assign('pageContent', $pageTpl->getContent());

		// Output our main layout
		    $mainTpl->display();


//EOF
?>