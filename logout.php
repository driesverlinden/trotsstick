<?php
	/**
	 * Logout page for TrotsStick
	 *
	 * @author	Andries Verlinden <andriesverlinden@gmail.com>
	 * @version	1.6
	 */

	 /**
	  * Log out
	  * --------------------------------------------------------------
	  */

		// start session (starts a new one, or continues the already started one)
		session_start();

		// unset all session vars
		foreach ($_SESSION as $key => $v) unset($_SESSION[$key]);

		// destroy the session
		session_destroy();

		// redirect to login
		header('location: login.php');
		exit();

		
//EOF
?>