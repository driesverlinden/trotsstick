<?php
	/**
	 * Login page for TrotsStick
	 *
	 * @author	Andries Verlinden <andriesverlinden@gmail.com>
	 * @version	1.6
	 */


        /**
	 * Includes
	 * ----------------------------------------------------------------
	 */

	    // config & functions
	    require_once './core/includes/config.php';
	    require_once './core/includes/functions.php';

	    // needed classes
	    require_once './core/includes/classes/database.php';
	    require_once './core/includes/classes/template.php';


	/**
	 * Database connection
	 * ----------------------------------------------------------------
	 */
	    $db = new TrotsStick(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	    $db->connect();


        /**
	 * Initial Values
	 * ----------------------------------------------------------------
	 */

	    // other form fields
	    $login = (isset($_POST['login']) ? htmlentities(stripPostSlashes($_POST['login'])) : '');
	    $pw	= (isset($_POST['pw']) ? htmlentities(stripPostSlashes($_POST['pw'])) : '');

	    // alreadyConfig
	    (bool) $alreadyConfig = false;

	    // database info
	    $users = $db->retrieveOne('SELECT * FROM users');

	    // clear error messages
	    $msgLogin = '';
	    $msgPw = '';

	    
	/**
	 * Check if the TrotsStick isn't configured already
	 * (only one user is allowed)
	 * ----------------------------------------------------------------
	 */

	    if (count($users) > 0) {
		$alreadyConfig = true;
	    }
		

	/**
	 * Start session and check if we are logged in
	 * ----------------------------------------------------------------
	 */
	    // start a session
	    session_start();
	    
	    // are we logged in or not
	    $loggedIn = isset($_SESSION['loggedin']) ? $_SESSION['loggedin'] : false; 

	    if ($loggedIn === true) {	
		header('location: index.php');
		exit();
	    }

	    
	/**
	 * Handle action 'btnOk' (user pressed login button)
	 * ----------------------------------------------------------------
	 */

	    if (isset($_POST['btnOk'])) {

		$userrecord = $db->retrieveOne('SELECT * FROM users WHERE name="' . $login . '"');

		if($pw == $userrecord['password']) {

		    // set loggedin state & username
		    $_SESSION['login'] = $login;
		    $_SESSION['loggedin'] = true;

		    // set a cookie with username and the password (expiration time within one week)
		    if((isset($_POST['autologin']))) {
			setcookie('login', $login);
		    }

		    // redirect to the index page, so that the cookie is set and will be read in
		    header('location: index.php');
		    exit();
		} else {
		    $msgLogin = 'de gebruiker bestaat niet!';
		    $msgPw = 'het wachtwoord is onjuist!';
		}
	    }

	    
        /**
	 * No action to handle: show our page itself
	 * ----------------------------------------------------------------
	 */

	    // Main Layout

		// load main layout into a template
		$mainTpl = new Template('./core/layout/layout.tpl');

		// asisgn vars in our main layout tpl
		$mainTpl->assign('pageTitle',	'TrotsStick - login');
		$mainTpl->assign('pageMeta',	'');
		$mainTpl->assign('pageCss',	'');
		$mainTpl->assign('pageJs',	'');
		$mainTpl->assign('pageH2', 	'Login');

	    // Page specific template

		// new template
		$pageTpl = new Template('./core/layout/login.tpl');

		// assign variables of the form
		$pageTpl->assign('formAction', $_SERVER['PHP_SELF']);

		// check for cookie
		if (isset($_COOKIE['login'])) {
		    $pageTpl->assign('login',   $_COOKIE['login']);
                    $pageTpl->assign('pw',      '');
		    $pageTpl->assignOption('oAutoLogin');
		} else {
		    $pageTpl->assign('login', $login);
		    $pageTpl->assign('pw', $pw);
		}

		// assign error messages
		$pageTpl->assign('msgPw', $msgPw);
		$pageTpl->assign('msgLogin', $msgLogin);

		// check if TrotsStick isn't configured already
		if ($alreadyConfig == true) {
		    $pageTpl->assignOption('oAlreadyConfig');
		} else {
		    $pageTpl->assignOption('oCreateStick');
		}

	    // Parse page specific layout into main layout
		$mainTpl->assign('pageContent', $pageTpl->getContent());

	    // Output our main layout
		$mainTpl->display();

		
//EOF
?>