<?php
	/**
	 * Wie ben ik? page for TrotsStick
	 *
	 * @author	Andries Verlinden <andriesverlinden@gmail.com>
	 * @version	1.8
	 */


        /**
	 * Includes
	 * ----------------------------------------------------------------
	 */

	    // config & functions
	    require_once './core/includes/config.php';
	    require_once './core/includes/functions.php';

	    // needed classes
	    require_once './core/includes/classes/database.php';
	    require_once './core/includes/classes/template.php';

	    
	/**
	 * Database connection
	 * ----------------------------------------------------------------
	 */
	    $db = new TrotsStick(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	    $db->connect();


        /**
	 * Start session and check if we are logged in
	 * ----------------------------------------------------------------
	 */
	    // start a session
	    session_start();

	    // are we logged in or not
	    $loggedIn = isset($_SESSION['loggedin']) ? $_SESSION['loggedin'] : false;
	    $user = isset($_SESSION['login']) ? $_SESSION['login'] : '';

	    if ($loggedIn === false) {
		header('location: index.php');
		exit();
	    }


	/**
	 * Initial Values
	 * ----------------------------------------------------------------
	 */

	    // member related params
	    $myMemberId = $user;
	    $myBasePage = '/red';
	    $myBaseDir = dirname(__FILE__) . '/members/' . $myMemberId . $myBasePage;
	    $myBaseUrl = 'members/' . $myMemberId . $myBasePage;
	    $basePic = 'core/img/noimage.jpg';

	    // user records
	    $name = '';
	    $street = '';
	    $city = '';
	    $phone = '';
	    $email = '';
	    $hobby = '';
	    $talent = '';
	    $you = '/you.jpg';
	    $family = '/family.jpg';
	    $family2 = '/family2.jpg';
	    $picHobby = '/hobby.jpg';
	    $picTalent = '/talent.jpg';

	    $optionFam2 = false;


	/**
	 * Get user records
	 * ----------------------------------------------------------------
	 */

	    // get database info
	    $userrecords = $db->retrieveOne('SELECT * FROM userrecords WHERE id="0"');

	    if (count($userrecords) > 0) {
		$name =	    $userrecords['name'];
		$street =   $userrecords['address'];
		$city =	    $userrecords['city'];
		$phone =    $userrecords['phone'];
		$email =    $userrecords['mail'];
		$hobby =    $userrecords['hobby'];
		$talent =   $userrecords['talent'];
	    }

	    // get photos
	    $path = $myBaseUrl;
	    $toReadDir = @opendir($path);

	    if (!file_exists($path . $you)) {
		$you = $basePic;
	    } else {
		$you = $path . $you;
	    }

	    if (!file_exists($path . $family)) {
		$family = $basePic;
	    } else {
		$family = $path . $family;
	    }

	    if (!file_exists($path . $family2)) {
		$optionFam2 = false;
	    } else {
		$optionFam2 = true;
		$family2 = $path . $family2;
	    }

	    if (!file_exists($path . $picHobby)) {
		$picHobby = $basePic;
	    } else {
		$picHobby = $path . $picHobby;
	    }

	    if (!file_exists($path . $picTalent)) {
		$picTalent = $basePic;
	    } else {
		$picTalent = $path . $picTalent;
	    }

	    @closedir($toReadDir);

	/**
	 * Delete pic
	 * -------------------------------------------------------------
	 */
	 
	 if (isset($_GET['delPhoto'])) {
	    $delFile = dirname(__FILE__) . '/' . $_GET['delPhoto'];
	    @unlink($delFile) or showError('unable to delete ' . $delFile);
	    header('Location:wiebenik.php');
	    exit(0);
	}

	/**
	 * No action to handle: show our page itself
	 * -------------------------------------------------------------
	 */
	    // Main Layout

		// load main layout into a template
		$mainTpl = new Template('./core/layout/layout.tpl');

		// asisgn vars in our main layout tpl
		$mainTpl->assign('pageTitle', 	'TrotsStick - Wie ben ik?');
		$mainTpl->assign('pageMeta',	'');
		
		if ($optionFam2 == true) {
		    $mainTpl->assign('pageCss', '
		dl {
		    width:  640px;
		}

		dl dt {
		    float: left;
		    clear: left;
		    width: 130px;
		    padding: 0 10px 0 0;
		    height: 15px;
		    line-height: 15px;
		}

		dl dt label {
			color: #000;
		}

		dl dd {
			float:  left;
			width: 500px;
			min-height: 15px;
			line-height: 15px;
			padding: 0 0 10px 0;
		}

		dl dd label {
			display: block;
			line-height: 15px;
		}

		p#hobby a img {
			top: 990px;
		}

		p#hobby a.delete {
			top: 995px;
		}

		p#talent a img {
			top: 1300px;
		}

		p#talent a.delete {
			top: 1305px;
		}');
		} else {
		    $mainTpl->assign('pageCss',	'
		dl {
		    width:  640px;
		}

		dl dt {
		    float: left;
		    clear: left;
		    width: 130px;
		    padding: 0 10px 0 0;
		    height: 15px;
		    line-height: 15px;
		}

		dl dt label {
			color: #000;
		}

		dl dd {
			float:  left;
			width: 500px;
			min-height: 15px;
			line-height: 15px;
			padding: 0 0 10px 0;
		}

		dl dd label {
			display: block;
			line-height: 15px;
		}');
		}
		
		$mainTpl->assign('pageJs',	'');
		$mainTpl->assign('pageH2',	'Wie ben ik?');

		// show logged in user
		if (($loggedIn == true)) {
		    $mainTpl->assignOption('oLoggedIn');
		    $mainTpl->assign('login', $_SESSION['login']);
		}

	    // Page specific template

		// new template
		$pageTpl = new Template('./core/layout/wiebenik.tpl');

		// formAction
		$pageTpl->assign('formAction', $_SERVER['PHP_SELF']);

		if (count($userrecords) > 0) {
		    $pageTpl->assignOption('oRecords');
		} else {
		    $pageTpl->assignOption('oNoRecords');
		}

		// user records
		if ($name == '') {
		    $pageTpl->assign('name', '&nbsp;');
		} else {
		    $pageTpl->assign('name', $name);
		}
		if ($street == '') {
		    $pageTpl->assign('street', '&nbsp;');
		} else {
		    $pageTpl->assign('street', $street);
		}
		if ($city == '') {
		    $pageTpl->assign('city', '&nbsp;');
		} else {
		    $pageTpl->assign('city', $city);
		}
		if ($phone == '') {
		    $pageTpl->assign('phone', '&nbsp;');
		} else {
		    $pageTpl->assign('phone', $phone);
		}
		if ($email == '') {
		    $pageTpl->assign('email', '&nbsp;');
		} else {
		    $pageTpl->assign('email', $email);
		}
		if ($hobby == '') {
		    $pageTpl->assign('hobby', '&nbsp;');
		} else {
		    $pageTpl->assign('hobby', $hobby);
		}
		if ($talent == '') {
		    $pageTpl->assign('talent', '&nbsp;');
		} else {
		    $pageTpl->assign('talent', $talent);
		}

		// pictures (std pic can't be deleted)
		$pageTpl->assign('you', $you);
		if ($you != $basePic) {
		    $pageTpl->assignOption('oDeleteYou');
		    $pageTpl->assign('urlDelYou', $_SERVER['PHP_SELF'] . '?delPhoto=' . urlencode($you));
		}
		$pageTpl->assign('family', $family);
		if ($family != $basePic) {
		    $pageTpl->assignOption('oDeleteFamily');
		    $pageTpl->assign('urlDelFamily', $_SERVER['PHP_SELF'] . '?delPhoto=' . urlencode($family));
		}
		if ($optionFam2 == true) {
		    $pageTpl->assignOption('oFamily2');
		    $pageTpl->assign('family2', $family2);
		    if ($family2 != $basePic) {
			$pageTpl->assignOption('oDeleteFamily2');
			$pageTpl->assign('urlDelFamily2', $_SERVER['PHP_SELF'] . '?delPhoto=' . urlencode($family2));
		    }
		}
		$pageTpl->assign('picHobby', $picHobby);
		if ($picHobby != $basePic) {
		    $pageTpl->assignOption('oDeleteHobby');
		    $pageTpl->assign('urlDelHobby', $_SERVER['PHP_SELF'] . '?delPhoto=' . urlencode($picHobby));
		}
		$pageTpl->assign('picTalent', $picTalent);
		if ($picTalent != $basePic) {
		    $pageTpl->assignOption('oDeleteTalent');
		    $pageTpl->assign('urlDelTalent', $_SERVER['PHP_SELF'] . '?delPhoto=' . urlencode($picTalent));
		}

	    // Parse page specific layout into main layout
		$mainTpl->assign('pageContent', $pageTpl->getContent());

	    // Output our main layout
		$mainTpl->display();

		
//EOF
?>