<?php
	/**
	 * Error page for TrotsStick
	 *
	 * @author	Andries Verlinden <andriesverlinden@gmail.com>
	 * @version	1.6
	 */


	/**
	 * Includes
	 * ----------------------------------------------------------------
	 */

	    // config & functions
	    require_once './core/includes/config.php';
	    require_once './core/includes/functions.php';

	    // needed classes
	    require_once './core/includes/classes/template.php';


	/**
	 * Initial Values
	 * ----------------------------------------------------------------
	 */

            $type = isset($_GET['type']) ? $_GET['type'] : 'unknown';
            $detail= isset($_GET['detail']) ? $_GET['detail'] : 'unknown';

            switch($type) {
                case 'createBaseDir':	$msgErr = 'kon de map ' . $detail . ' niet aanmaken. Controleer de rechten op de map en/of van de gebruiker.';
					break;
		case 'readingBaseDir':	$msgErr = 'kon de map ' . $detail . ' niet inlezen. Controleer de rechten op de map en of van de gebruiker.';
					break;
		case 'cantCopy':	$msgErr = 'het bestand ' . $detail . ' kon niet gekopieerd worden.';
					break;
		case 'notExists':	$msgErr = 'het bestand ' . $detail . ' bestaat niet en kan dus niet verwijderd worden.';
					break;
		case 'delete':		$msgErr = 'het bestand ' . $detail . ' kon niet worden verwijderd. Controleer de rechten op de map en/of van de gebruiker en of het bestand bestaat.';
					break;
		case 'cantUploadFile':	$msgErr = 'het bestand ' . $detail . ' kon niet worden geüpload. Controleer de rechten op de map en/of van de gebruiker en of het bestand bestaat.';
					break;
		case 'cantSelectUsersDB':  $msgErr = 'de user databank kon niet gevonden worden op ' . $detail;
					break;
                default:    $msgErr = 'Er heeft zich een onbekende fout voor gedaan.';
            }


	/**
	 * No action to handle: show our page itself
	 * ----------------------------------------------------------------
	 */

	    // Main template

		// load main layout into a template
		$mainTpl = new Template('./core/layout/layout.tpl');

		// asisgn vars in our main layout tpl
		$mainTpl->assign('pageTitle', 	'Todolist - Error');
		$mainTpl->assign('pageMeta', 	'');
		$mainTpl->assign('pageCss',	'');
		$mainTpl->assign('pageJs',	'');
		$mainTpl->assign('pageH2',	'Errorpagina');

	    // Page specific template

		// Create new template
		    $pageTpl = new Template('./core/layout/error.tpl');

		// assign basic variables of the form
		    $pageTpl->assign('msgErr', $msgErr);

	    // Parse page specific layout into main layout
		$mainTpl->assign('pageContent', $pageTpl->getContent());


	    // Output our main layout
		$mainTpl->display();

//EOF

?>