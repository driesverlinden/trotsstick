<?php
	/**
	 * Index page for TrotsStick
	 *
	 * @author	Andries Verlinden <andriesverlinden@gmail.com>
	 * @version	1.6
	 */


        /**
	 * Includes
	 * ----------------------------------------------------------------
	 */

	    // config & functions
	    require_once './core/includes/config.php';
	    require_once './core/includes/functions.php';

	    // needed classes
	    require_once './core/includes/classes/template.php';


	/**
	 * Initial Values
	 * ----------------------------------------------------------------
	 */

	    // start session (starts a new one, or continues the already started one)
	    session_start();

	    // check if we are logged in or not
	    $loggedIn = isset($_SESSION['loggedin']) ? $_SESSION['loggedin'] : false;

	    if ($loggedIn === false) {	// not logged in
		header('location: login.php');
		exit();
	    }


        /**
	 * No action to handle: show our page itself
	 * ----------------------------------------------------------------
	 */

	    // Main Layout

		// load main layout into a template
		$mainTpl = new Template('./core/layout/layout.tpl');

		// asisgn vars in our main layout tpl
		$mainTpl->assign('pageTitle',	'TrotsStick - Rekenknap');
		$mainTpl->assign('pageMeta',	'');
		$mainTpl->assign('pageCss',	'');
		$mainTpl->assign('pageJs',	'');
		$mainTpl->assign('pageH2',	'Rekenknap');

		// show logged in user
		if (($loggedIn == true)) {
		    $mainTpl->assignOption('oLoggedIn');
		    $mainTpl->assign('login', $_SESSION['login']);
		}

	    // Page specific template

		// make new template
		$pageTpl = new Template('./core/layout/reken.tpl');

	    // Parse page specific layout into main layout
		$mainTpl->assign('pageContent', $pageTpl->getContent());

	    // Output our main layout
		$mainTpl->display();

//EOF
?>