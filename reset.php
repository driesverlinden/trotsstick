<?php
	/**
	 * Reset page for TrotsStick
	 *
	 * @author	Andries Verlinden <andriesverlinden@gmail.com>
	 * @version	1.8
	 */


        /**
	 * Includes
	 * ----------------------------------------------------------------
	 */

	    // config & functions
	    require_once './core/includes/config.php';
	    require_once './core/includes/functions.php';

	    // needed classes
	    require_once './core/includes/classes/database.php';
	    require_once './core/includes/classes/template.php';


	/**
	 * Database connection
	 * ----------------------------------------------------------------
	 */
	    $db = new TrotsStick(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	    $db->connect();


        /**
	 * Initial Values
	 * ----------------------------------------------------------------
	 */

	    // start session (starts a new one, or continues the already started one)
	    session_start();

	    // information
	    $step = 1;
	    $name =	    (isset($_POST['name']) ? $_POST['name'] : '');
	    $question =	    '';
	    $answer =	    (isset($_POST['answer']) ? $_POST['answer'] : '');
	    $newPw =	    (isset($_POST['newPw']) ? $_POST['newPw'] : '');

	    // error messages
	    $msgName = '';
	    $msgAnswer = '';
	    $msgNewPw = '';


	/**
	 * Request question
	 * -----------------------------------------------------------------
	 */

	    if (isset($_POST['btnSendName'])) {
		$question = $db->retrieveOne('SELECT question from USERS WHERE name="' . $name . '"');
		if (count($question) == '') {
		    $msgName = 'De gebruikersnaam werd niet terug gevonden.';
		} else {
		    $step = 2;
		}
	    }

	    
	/**
	 * Reset password
	 * -----------------------------------------------------------------
	 */

	    if (isset($_POST['btnReset'])) {
		$rightanswer = $db->retrieveOne('SELECT answer from USERS');
		if ($answer == $rightanswer['answer']) {
		    $step = 3;
		} else {
		    $msgAnswer = 'Je antwoord is verkeerd';
		}
	    }


	/**
	 * Change password
	 * -----------------------------------------------------------------
	 */

	    if (isset($_POST['btnRenew'])) {
		if (checkSecurePw($newPw)) {
		    $values = array('password' => $newPw);
		    $db->update('users', $values, 'name = "' . $name . '"');
		    $step = 4;
		} else {
		    $msgNewPw = 'Je nieuw wachtwoord is niet veilig genoeg!';
		}
		
	    }

	/**
	 * No action to handle: show our page itself
	 * -----------------------------------------------------------------
	 */

	    // Main Layout

		// load main layout into a template
		$mainTpl = new Template('./core/layout/layout.tpl');

		// asisgn vars in our main layout tpl
		$mainTpl->assign('pageTitle', 	'TrotsStick');
		$mainTpl->assign('pageMeta', 	'');
		$mainTpl->assign('pageCss',	'');
		$mainTpl->assign('pageJs',	'');
		$mainTpl->assign('pageH2', 	'Wachtwood resetten');

	    // Page specific template

		// new template
		$pageTpl = new Template('./core/layout/reset.tpl');
		
		$pageTpl->assign('formAction', $_SERVER['PHP_SELF']);
		
		if ($step == 1) {
		    $pageTpl->assignOption('oStep1');
		    $pageTpl->assign('msgName',    $msgName);		    
		} else if ($step == 2) {
		    $pageTpl->assignOption('oStep2');
		    $pageTpl->assign('name',   $name);
		    $pageTpl->assign('question',    $question['question']);
		    $pageTpl->assign('msgAnswer',   $msgAnswer);
		} else if ($step == 3) {
		    $pageTpl->assignOption('oStep3');
		    $pageTpl->assign('name',   $name);
		    $pageTpl->assign('msgNewPw',    $msgNewPw); 
		} else if ($step == 4) {
		    $pageTpl->assignOption('oStep4');
		} else {
		    $pageTpl->assignOption('oStep1');
		    $pageTpl->assign('msgName',    $msgName);
		}


	    // Parse page specific layout into main layout

		$mainTpl->assign('pageContent', $pageTpl->getContent());

	    // Output our main layout

		$mainTpl->display();

//EOF
?>