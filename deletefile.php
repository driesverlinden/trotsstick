<?php
	/**
	 * Delete a file page for TrotsStick
	 *
	 * @author	Andries Verlinden <andriesverlinden@gmail.com>
	 * @version	1.6
	 */


        /**
	 * Includes
	 * ----------------------------------------------------------------
	 */

	    // config & functions
	    require_once './core/includes/config.php';
	    require_once './core/includes/functions.php';

	    // needed classes
	    require_once './core/includes/classes/template.php';

        /**
	 * Initial Values
	 * ----------------------------------------------------------------
	 */

	    // start session (starts a new one, or continues the already started one)
	    session_start();

	    // check if we are logged in or not
	    $loggedIn = isset($_SESSION['loggedin']) ? $_SESSION['loggedin'] : false;

	    if ($loggedIn === false) {	// not logged in
		header('location: login.php');
		exit();
	    }

	    // member related params
	    $myMemberId = isset($_SESSION['login']) ? $_SESSION['login'] : 'stranger';
	    $myBaseDir = dirname(__FILE__) . '/members/' . $myMemberId;
	    $myBaseUrl = 'members/' . $myMemberId;

	    // file to delete
	    $file = isset($_GET['file']) ? $_GET['file'] : '';
	    $delUrl = $myBaseDir . '/' . $file;


	/**
	 * Delete file
	 * -----------------------------------------------------------------
	 */
	    if (isset($_POST['btnDelete'])) {
		// check if file exists
		if (!file_exists($delUrl)) showError('notExists', $file);

		// delete it
		@unlink($delUrl) or showError('delete', $file);

		// redirect
		header('location: deletefile.php?delete=yes');
		exit();
	    }
	    

	/**
	 * No action to handle: show our page itself
	 * -----------------------------------------------------------------
	 */

	    // Main Layout

		// load main layout into a template
		$mainTpl = new Template('./core/layout/layout.tpl');

		// asisgn vars in our main layout tpl
		$mainTpl->assign('pageTitle', 	'TrotsStick - verwijder een bestand');
		$mainTpl->assign('pageMeta',	'');
		$mainTpl->assign('pageCss',	'');
		$mainTpl->assign('pageJs', 	'');
		$mainTpl->assign('pageH2',	'Verwijder een bestand');

		// show logged in user
		if (($loggedIn == true)) {
		    $mainTpl->assignOption('oLoggedIn');
		    $mainTpl->assign('login', $_SESSION['login']);
		}

	    // Page specific template

		// new template
		$pageTpl = new Template('./core/layout/deletefile.tpl');

		// assign variables of the form
		$pageTpl->assign('formAction', 	$_SERVER['PHP_SELF'] . '?file=' . $file);

		// file selected ?
		if ($file != '') {
		    $pageTpl->assignOption('oDelete');
		    $pageTpl->assign('urlFile', $file);
		} else {
		    if (isset($_GET['delete'])) {
			$pageTpl->assignOption('oDeleted');
		    } else {
			$pageTpl->assignOption('oNoFileSelected');
		    }
		}

	    // Parse page specific layout into main layout

		$mainTpl->assign('pageContent', $pageTpl->getContent());

	    // Output our main layout

		$mainTpl->display();

		
//EOF
?>