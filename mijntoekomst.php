<?php
	/**
	 * Mijn toekomst page for TrotsStick
	 *
	 * @author	Andries Verlinden <andriesverlinden@gmail.com>
	 * @version	1.6
	 */


        /**
	 * Includes
	 * ----------------------------------------------------------------
	 */


	    // config & functions
	    require_once './core/includes/config.php';
	    require_once './core/includes/functions.php';

	    // needed classes
	    require_once './core/includes/classes/database.php';
	    require_once './core/includes/classes/template.php';


	/**
	 * Database connection
	 * ----------------------------------------------------------------
	 */
	    $db = new TrotsStick(DB_HOST, DB_USER, DB_PASS, DB_NAME);
	    $db->connect();


        /**
	 * Initial Values
	 * ----------------------------------------------------------------
	 */

	    // start session (starts a new one, or continues the already started one)
	    session_start();

	    // check if we are logged in or not
	    $loggedIn = isset($_SESSION['loggedin']) ? $_SESSION['loggedin'] : false;

	    if ($loggedIn === false) {	// not logged in
		header('location: login.php');
		exit();
	    }

	    // member related params
	    $myMemberId = isset($_SESSION['login']) ? $_SESSION['login'] : 'stranger';
	    $myBasePage = '/orange';
	    $myBaseDir = dirname(__FILE__) . '/members/' . $myMemberId . $myBasePage;
	    $myBaseUrl = 'members/' . $myMemberId . $myBasePage;

	    // files
	    $documents = array();
	    $pictures = array();
	    $movies = array();
	    $sounds = array();

	    // Whey boxes
	    $showEdit = false;
	    $showEditOthers = false;
	    $whey = isset($_POST['whey']) ? $_POST['whey'] : '';
	    $wheyOthers = isset($_POST['wheyOthers']) ? $_POST['wheyOthers'] : '';

	    // declare error messages
	    $msgWhey = '';
	    $msgWheyOthers = '';
	    $msgUpload = '';


	/**
	 * Get files
	 * ----------------------------------------------------------------
	 */

	    // check base directory
	    if (!file_exists($myBaseDir)) {
		    @mkdir($myBaseDir) or showError('createBaseDir', $myBaseDir);
	    }

	    // open base directory
	    $dp = opendir($myBaseDir) or showError('readingBaseDir', $myBaseDir);

	    // read base directory
	    while (($file = readdir($dp)) !== false) {
		if ($file == '.') continue;
		if ($file == '..') continue;
		if (is_dir($myBaseDir.'/'.$file)) continue;
		// saves pictures, documents, movies and sounds
		if (isPic($file)) {
		    $pictures[] = $file;
		    continue;
		}
		if (isDoc($file)) {
		    $documents[] = $file;
		    continue;
		}
		if (isMovie($file)) {
		    $movies[] = $file;
		    continue;
		}
		if (isSound($file)) {
		    $sounds[] = $file;
		    continue;
		}
		continue;
	    }

	    // close base directory pointer
	    closedir($dp);


	/**
	 * Get drive info
	 * ----------------------------------------------------------------
	 */
	    // free disk space
	    $free = round(disk_free_space(DRIVE)/1024/1024/1024,2);

	    if ($free < 1) {
		$msgUpload = 'Opgelet! Je TrotsStick is bijna vol! <br /><a href="about.php">Controleer de grote van je USB-stick</a>';
	    }

	    
	/**
	 * Upload file
	 * -----------------------------------------------------------------
	 */
	    if (!empty($_FILES['upload'])) {

		$fileToCopy = $_FILES['upload']['tmp_name'];
		$fileName   = $_FILES['upload']['name'];
		$fileSize   = $_FILES['upload']['size'];

		if (isSizeAcceptable($fileSize) == true) {
		    echo $fileSize;
		    if (isLengthAcceptable($fileName)) {
			if (isExAcceptable($fileName)) {
			    $copyTo = $myBaseDir . '/' . $fileName;
			    @move_uploaded_file($fileToCopy, $copyTo) or $msgUpload = 'het bestand ' . $fileName . ' kan niet gekopiërd worden naar '.$myBaseDir;
			    header('Location: mijntoekomst.php');
			    exit(0);
			} else {
			    $msgUpload = "je kan enkel bestanden met de extensie's .doc(x), .xls(x), .ppt(x), .pdf, .jpg, .jpeg, .png, .gif, .mp4, .wmv, .mov, .mp3, .wav uploaden.";
			}
		    } else {
			$msgUpload = "De bestandsnaam mag slechts 42 tekens bevatten!";
		    }
		} else {
		    $msgUpload = "Het bestand dat je uploadt mag maar maximum 32MB groot zijn!";
		}
	    }


	/**
	 * Send / get whey
	 * ----------------------------------------------------------------
	 */

		if (isset($_POST['btnShowEdit'])) {
		    $showEdit = true;
		}

		if (isset($_POST['btnEdit'])) {
		    $values = array('mijn_toekomst' => stripPostSlashes($whey));
		    $db->update('whey', $values, 'id=0');
		    header('location: mijntoekomst.php');
		    exit(0);
		}

		$whey = $db->retrieveOne('SELECT mijn_toekomst FROM whey');

	/**
	 * Send / get whey others
	 * ----------------------------------------------------------------
	 */

		if (isset($_POST['btnShowEditOthers'])) {
		    $showEditOthers = true;
		}

		if (isset($_POST['btnEditOthers'])) {
		    $values = array('mijn_toekomst' => stripPostSlashes($wheyOthers));
		    $db->update('wheyothers', $values, 'id=0');
		    header('location: mijntoekomst.php');
		    exit(0);
		}

		$wheyOthers = $db->retrieveOne('SELECT mijn_toekomst FROM wheyothers');
	    
	/**
	 * No action to handle: show our page itself
	 * ----------------------------------------------------------------
	 */
	    // Main Layout

		// load main layout into a template
		$mainTpl = new Template('./core/layout/layout.tpl');

		// asisgn vars in our main layout tpl
		$mainTpl->assign('pageTitle', 	'TrotsStick - Mijn toekomst');
		$mainTpl->assign('pageMeta',	'');
		$mainTpl->assign('pageCss',	'.documents h3, .pictures h3, .sounds h3, .movies h3 {
		background-color:  #ff7000;
		color: #FFF;
		}
		#myfuture a {
		    color: #ff7000;
		    text-decoration: none;
		}
		#text {
		    background-color: #FFF;

		}
		#formWhey, #formOthers {
		    float: left;
		}');
		$mainTpl->assign('pageJs',	'
	    <script type="text/javascript" src="core/js/uploadchecking.js"></script>
	    <!-- TinyMCE -->
	    <script type="text/javascript" src="core/js/tiny_mce/tiny_mce.js"></script>
	    <script type="text/javascript">
		tinyMCE.init({
		    mode : "textareas", 
		    theme : "advanced",
			theme_advanced_buttons1_add : "fontselect, bullist, numlist, indent, outdent, undo, redo, cleanup, removeformat",
		    theme_advanced_buttons2 : "",
		    theme_advanced_buttons3 : "",
		    theme_advanced_disable : "link, unlink, sub, sup, seperator, code, image, hr, anchor, formatselect, styleselect, fontsizeselect, forecolor, backcolor, forecolorpicker, backcolorpicker, charmap, visualaid, newdocument, blockquote, help",
		    theme_advanced_toolbar_location : "top",
		    theme_advanced_toolbar_align : "left",
		    theme_advanced_fonts : "Andale Mono=andale mono,times;" + "Arial=arial,helvetica,sans-serif;" + "Arial Black=arial black,avant garde;" + "Book Antiqua=book antiqua,palatino;" + "Comic Sans MS=comic sans ms,sans-serif;" + "Courier New=courier new,courier;" + "Georgia=georgia,palatino;" + "Helvetica=helvetica;" + "Impact=impact,chicago;" + "Symbol=symbol;" + "Tahoma=tahoma,arial,helvetica,sans-serif;" + "Terminal=terminal,monaco;" + "Times New Roman=times new roman,times;" + "Trebuchet MS=trebuchet ms,geneva;" + "Verdana=verdana,geneva;" + "Webdings=webdings;" + "Wingdings=wingdings,zapf dingbats"
		});
	    </script>
	    <!-- /TinyMCE -->');
		$mainTpl->assign('pageH2',	'Mijn toekomst');

		// show logged in user
		if (($loggedIn == true)) {
		    $mainTpl->assignOption('oLoggedIn');
		    $mainTpl->assign('login', $_SESSION['login']);
		}

	    // Page specific template

		// new template
		$pageTpl = new Template('./core/layout/mijntoekomst.tpl');

		// formAction
		$pageTpl->assign('formAction', $_SERVER['PHP_SELF']);
		$pageTpl->assign('formActionUpload', $_SERVER['PHP_SELF']);
		$pageTpl->assign('msgUpload', $msgUpload);

		// Whey
		if ($showEdit == true) {
		    $pageTpl->assignOption('oEditWhey');
		} else {
		    $pageTpl->assignOption('oWhey');
		}
		$pageTpl->assign('wheyTXT', $whey['mijn_toekomst']);
		$pageTpl->assign('msgWhey', $msgWhey);

		// Whey others
		if ($showEditOthers == true) {
		    $pageTpl->assignOption('oEditOthers');
		} else {
		    $pageTpl->assignOption('oOthers');
		}
		$pageTpl->assign('wheyTXTOthers', $wheyOthers['mijn_toekomst']);
		$pageTpl->assign('msgWheyOthers', $msgWheyOthers);

		// Documents
		    // set iteration
		    $pageTpl->setIteration('iDocuments');

		    // loop all collections and output them
		    foreach ($documents as $document) {
			$pageTpl->assignIteration('hrefDoc', 'openfile.php?type=doc&file=' . $myBasePage . '/' . urlencode($document));
			$pageTpl->assignIteration('urlDel', 'deletefile.php?file=' . $myBasePage . '/' . urlencode($document));
			$pageTpl->assignIteration('nameDoc', $document);
			//refill iteration
			$pageTpl->refillIteration('');
		    }
		    // parse iteration
		    $pageTpl->parseIteration('');

		// Pictures
		    // set iteration
		    $pageTpl->setIteration('iPictures');

		    // loop all collections and output them
		    foreach ($pictures as $picture) {
			$pageTpl->assignIteration('hrefPic', 'openfile.php?type=pic&file=' . $myBasePage . '/' . urlencode($picture));
			$pageTpl->assignIteration('urlDel', 'deletefile.php?file=' . $myBasePage . '/' . urlencode($picture));
			$pageTpl->assignIteration('namePic', $picture);
			//refill iteration
			$pageTpl->refillIteration('');
		    }
		    // parse iteration
		    $pageTpl->parseIteration('');

		// Movies
		    // set iteration
		    $pageTpl->setIteration('iMovies');

		    // loop all collections and output them
		    foreach ($movies as $movie) {
			$pageTpl->assignIteration('hrefMovie', 'openfile.php?type=mov&file=' . $myBasePage . '/' . urlencode($movie));
			$pageTpl->assignIteration('urlDel', 'deletefile.php?file=' . $myBasePage . '/' . urlencode($movie));
			$pageTpl->assignIteration('nameMovie', $movie);
			//refill iteration
			$pageTpl->refillIteration('');
		    }
		    // parse iteration
		    $pageTpl->parseIteration('');

		// Sounds
		    // set iteration
		    $pageTpl->setIteration('iSounds');

		    // loop all collections and output them
		    foreach ($sounds as $sound) {
			$pageTpl->assignIteration('hrefSound', 'openfile.php?type=sou&file=' . $myBasePage . '/' . urlencode($sound));
			$pageTpl->assignIteration('urlDel', 'deletefile.php?file=' . $myBasePage . '/' . urlencode($sound));
			$pageTpl->assignIteration('nameSound', $sound);
			//refill iteration
			$pageTpl->refillIteration('');
		    }
		    // parse iteration
		    $pageTpl->parseIteration('');

		// Whey
		if (file_exists($myBaseDir . '/mijntoekomst.txt')) {
		    $txtFile = $myBaseDir . '/mijntoekomst.txt';
		    $fh = fopen($txtFile, 'rb') or $msgWhey = 'het bestand ' . $txtFile . ' kan niet geopend worden.';
		    $whey = fread($fh, filesize($txtFile));
		    $pageTpl->assign('wheyTXT', $whey);
		    fclose($fh);
		} else {
		    $pageTpl->assign('wheyTXT', '');
		}

	    // Parse page specific layout into main layout
		$mainTpl->assign('pageContent', $pageTpl->getContent());

	    // Output our main layout
		$mainTpl->display();

		
//EOF
?>